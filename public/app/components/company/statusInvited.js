angular.module('app')
    .component('statusInvited', {
        templateUrl: '/app/templates/company/statusInvitedEmployee.html',
        controller: function(statusInvitedService, $location) {
            var self = this;
            self.translates = translates;
            var reServ = /^.*\/(.*)$/;
            var id = $location.url().replace(reServ, '$1');



            statusInvitedService.getAll(id).then(function(data){
                var result = data.data;
                self.statuses = {};

                for(employee in result) {
                    for(key in result[employee]) {
                        switch (result[employee][key]) {
                            case 'reject':
                                self.statuses[employee] = 'btn-warning';
                                break;
                            case 'fire':
                                self.statuses[employee] = 'btn-danger';
                                break;
                            case 'success':
                                self.statuses[employee] = 'btn-success';
                                break;
                            case 'wait':
                                self.statuses[employee] = 'btn-info';
                                break;
                        }

                    }
                }

                self.employees = result;
            });


        }
    })