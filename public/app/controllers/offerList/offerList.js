angular
    .module('app')
    .controller('offerList', offerList);

offerList.$inject=['$http', 'requestService'];


function offerList($http ,requestService){
    var vm = this;
    vm.offers = [];
    vm.offerResponses = [];

    requestService.getOffersInfo().then(function(data){
       
        vm.offers = data.OfferEntityListJS;
        vm.offerResponses = data.OfferResponseEntityListJS;
        vm.offers.forEach(function(offer){
            offer.items = JSON.parse(offer.items);
            offer.responses = vm.offerResponses.filter(function(item){
                return item.offer_id == offer.id;
            });
            
            offer.isReserve = false;
	          data.UserOfferEntityListJS.forEach(function(userOffer){
	          	if(userOffer.offer_id == offer.id){
	          		offer.status = userOffer;
	          	}
	          });
        });
        console.log('getOffersInfo', vm.offers);
    });
};

