angular.module('app')
    .controller('requestList', requestList);

requestList.$inject = ['$scope', 'requestService', '$location', '$filter'];

function requestList($scope, requestService, $location, $filter){
    var reServ = /^.*\/(.*)$/;
    var id = $location.url().replace(reServ, '$1');
    var searchObject = $location.search();
    $scope.sortType     = 'name';
    $scope.sortReverse  = false;
    $scope.requestsMaped = [];
    $scope.offers = [];
    $scope.offerResponses = [];
    $scope.isReserved = searchObject.reserved;
    $scope.isExchange = searchObject.exchange;

    requestService.getAll(id).then(function(data){
        $scope.requests = data;
        $scope.requests.forEach(function(item){
            if(!item.offers){
                return;
            }
            item.offers.forEach(function(offers){
                if(offers.response){
                    offers.response.forEach(function(resp){
                        resp.price = JSON.parse(resp.price);
                    });
                }
            });
        });
    });
}