angular
    .module('app')
    .controller('offerList', offerList);

offerList.$inject=['$http', 'requestService', '$location'];


function offerList($http ,requestService, $location){
    var reServ = /^.*\/(.*)$/;
    var id = $location.url().replace(reServ, '$1');

    var vm = this;
    vm.offers = [];
    vm.offerResponses = [];

    requestService.get('/ru/api/offer/user/'+ id).then(function(data){
        console.log('getOffersInfo', data);
        vm.offers = data.OfferEntityListJS;
        vm.offerResponses = data.OfferResponseEntityListJS;
        vm.offers.forEach(function(offer){
            offer.responses = vm.offerResponses.filter(function(item){
                return item.offer_id == offer.id;
            });
        });
    });
}