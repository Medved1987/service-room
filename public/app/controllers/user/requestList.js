angular.module('app')
    .controller('userRequestList', userRequestList);

userRequestList.$inject = ['$scope', 'requestService', '$location'];

function userRequestList($scope, requestService, $location){
    var reServ = /^.*\/(.*)$/;
    var id = user_id;
    var searchObject = $location.search();
    var url = '/ru/api/offer/user/'+ id;
    $scope.sortType     = 'name';
    $scope.sortReverse  = false;
    $scope.requestsMaped = [];
    $scope.offers = [];
    $scope.offerResponses = [];
    $scope.isReserved = searchObject.reserved;
    $scope.isExchange = searchObject.exchange;

    function isReserved(){}

    requestService.get(url).then(function(data){
    	$scope.offers = data.OfferEntityListJS;
    	$scope.offerResponses = data.OfferResponseEntityListJS;
    	$scope.offers.forEach(function(offer){
            offer.responses = $scope.offerResponses.filter(function(item){
                return item.offer_id == offer.id;
            });
            offer.isReserve = false;
            data.UserOfferEntityListJS.forEach(function(userOffer){
            	if(userOffer.offer_id == offer.id){
            		offer.status = userOffer;
            	}
            });
        });
        console.log($scope.offers)
    });
}