angular.module('app')
    .controller('usersList', ['$scope', 'usersService', function($scope, usersService) {

            usersService.getAll().then(function(data){
                $scope.users = data.data;
            });

            $scope.sortType     = 'name';
            $scope.sortReverse  = false;
    }])