angular
    .module('app')
    .service('statusInvitedService', function($http) {
        return {
            getAll: function(id){
                return $http.get('/ru/company/api/status/get/'+ id)
            }
        }

    })