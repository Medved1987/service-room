angular
    .module('app')
    .service('requestService', requestService);

requestService.$inject = ['$q', '$http'];

function requestService($q, $http) {
    return {
        getAll: function(id){
            return this.get('/ru/company/api/request/' + id);
        },
        getOffersInfo: function (){
            return this.get('/ru/api/offerList');
        },
        get: function(url){
            return $q(function(resolve, reject){
                $http.get(url).then(function(data){
                    resolve(data.data);
                }, function(error){
                    reject(error);
                });
            });
        }
    }
}