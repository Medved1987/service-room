$(function(){
	/* Вкл/выкл все поля */
	$('#chk').on('click', function(){
		if($(this).hasClass('on')){
    		$('.form-check-input').prop('checked', false);
    		$('.form-control').prop('disabled', true).val(null);
    		$(this).removeClass('on')
    			   .addClass('off')
    			   .text('Вкл. всё');
		}else if($(this).hasClass('off')){
			$('.form-check-input').prop('checked', true);
    		$('.form-control').prop('disabled', false);
    		$('.value').val(0);
			$(this).removeClass('off')
			       .addClass('on')
			       .text('Выкл. всё');
		}
	});
	/* Вкл/выкл одно поле */
	$(".form-check-input").on('change', function(){
		var id = $(this).attr('id');
		id = id.substring(4);
		var input = $('#'+id+'_value');
		var inputMin = $('#'+id+'_value_min');
		if(input.prop('disabled') == false){
			$(input).prop('disabled', true);
			$(input).attr('required', false);
			$(input).val(null);
			$(inputMin).prop('disabled', true);
			$(inputMin).val(null);
		}else{
			$(input).prop('disabled', false);
			$(input).attr('required', 'required');
			$(input).val(0);
			$(inputMin).prop('disabled', false);
		}
	});
});