$(function(){
	checkFields();
});

function checkFields(){
	if($('form > fieldset').children().length <= 3){
		$("#remove-button").attr('disabled', true);
	}else{
		$("#remove-button").attr('disabled', false);
	}
}

function addFields(){
	var currentCount = $('form > fieldset > fieldset').length;
	var template = $('form > fieldset > span').data('template');
 	template = template.replace(/__index__/g, currentCount);

 	$('form > fieldset').append(template);
 	checkFields();
 	return false;
}

function removeFields(){
     var lastChild = $('form > fieldset > fieldset').last();
     $(lastChild).remove();
     checkFields();
     return false;
}