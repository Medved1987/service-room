$(function(){
	/* Вкл/выкл все поля */
	$('#chk').on('click', function(){
		if($(this).hasClass('on')){
    		$('.form-check-input').prop('checked', false);
    		$('.form-control').prop('disabled', true);
    		$('.form-control').val(null);
    		$(this).removeClass('on')
    			   .addClass('off')
    			   .text('Вкл. всё');
		}else if($(this).hasClass('off')){
			$('.form-check-input').prop('checked', true);
    		$('.form-control').prop('disabled', false);
    		$('.form-control').val(0);
			$(this).removeClass('off')
			       .addClass('on')
			       .text('Выкл. всё');
		}
	});
	
	/* Вкл/выкл одно поле */
	$(".form-check-input").on('change', function(){
		var id = $(this).attr('id');
		id = id.substring(4);
		var input = $('#'+id);
		if(input.prop('disabled') == false){
			$(input).prop('disabled', true);
			$(input).attr('required', false);
			$(input).val(null);
		}else{
			$(input).prop('disabled', false);
			$(input).attr('required', 'required');
			$(input).val(0);
		}
	});
});