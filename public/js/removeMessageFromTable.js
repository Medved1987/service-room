$(function () {
    $('body').on('click', '.confirm', function (e) {
        var c = confirm(confirmMsg);
        e.preventDefault();
        if (c == false) {
            return false;
        } else {
            var url = $(this).attr('href');
            $.post(url, function (data) {
                var status = 'error';
                if (data.result) {
                    status = 'success';
                    var tr = $('#' + data.id);
                    tr.fadeOut(function () {
                        $(this).remove();
                    });
                }
                var msg = data.msg;
                var alert = '<div class="error">' +
                    '<div class="alert alert-dismissible alert-' + status + '">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">' +
                    '×' +
                    '</button>' +
                    '<ul class="notification-flash-list">' +
                    '<li>' + msg + '</li>' +
                    '</ul>' +
                    '</div>' +
                    '</div>';
                $(alert).appendTo(".notification").fadeIn();
            });
        }
    });
});