(function(){
    'use strict';


    angular
        .module('profileContactsList')
        .controller('profileContactsListController', profileContactsListController);

    profileContactsListController.$inject = ['profileHttpService', '$location'];

    function profileContactsListController(profileHttpService, $location){
        var reServ = /^.*\/(.*)$/;
        var id = $location.url().replace(reServ, '$1');
        var userId = user_id;
        var vm = this;

        vm.phones = [];
        vm.skypes = [];
        vm.emails = [];

        vm.offers = [];
        vm.offerResponses = [];
        vm.userOfferList = [];

        getContactsInfo();

        function getContactsInfo(){
            profileHttpService.getPhones(userId).then(function(data){
                vm.phones = data.data;
            });
            profileHttpService.getSkypes(userId).then(function(data){
                vm.skypes = data.data;
            });
            profileHttpService.getEmails(userId).then(function(data){
                vm.emails = data.data;
            });

            profileHttpService.get('/ru/api/offer/user/'+ userId).then(function(data){
                if(!data.OfferResponseEntityListJS.length){
                    return;
                }
                vm.offers = data.OfferEntityListJS;
                vm.offerResponses = data.OfferResponseEntityListJS;
                vm.offers.forEach(function(offer){
                    offer.responses = vm.offerResponses.filter(function(item){
                        return item.offer_id == offer.id;
                    });
                });

            });
        }




    }
})();
