(function(){
	'use strict';

	angular.module('cabinet')
	   .directive("skypeEditor", skypeEditorDirective);

	function skypeEditorDirective(){
		return {
			controllerAs: "skEd",
			restrict: "E",
			templateUrl: "/user-profile-angular-fragments/userSkypes.html",
			controller: skypeEditorController	
		};
	}

	skypeEditorController.$inject = ['$http' , 'profileModal', 'profileHttpService'];

	function skypeEditorController($http, profileModal, profileHttpService){
		var vm = this;
		var userId = user_id;
		var url = '/'+ lang + '/user/api/skype/' + userId;
		vm.skypeList = [];
		vm.newSkype = '';
		vm.validationFormServerMsg = '';

		vm.getSkypes = getSkypes;
		vm.addNew = addNew;
		vm.save = save;
		vm.del = del;
		
		//Init
		vm.getSkypes();				

		function getSkypes(){
			profileHttpService.getSkypes(userId).then(function(data){
				vm.skypeList = data.data;
				vm.skypeList.forEach(function(item){
					item.skype_old = item.skype;
					item.isValid = true;
				});
			}, function(err){
				console.log(err);
			});
		}

		function addNew(){
			//Adding input change watcher
			if(!vm.addNewSkype.newSkype.$viewChangeListeners.length){
				vm.addNewSkype.newSkype.$viewChangeListeners.push(function(){
					vm.addNewSkype.newSkype.$setValidity('validationFormServer', true);
				});
			}
			if(!vm.addNewSkype.$valid){							
				return;
			}
			$http({
					method: "POST",
					url:    url + '/post',
					headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
					data: $.param({skype: vm.newSkype})					
				}).success(function(data){
					if(data[0] == 1){
						vm.skypeList.push({
							user_id : null,
							skype : vm.newSkype,
							skype_old : vm.newSkype,
							isValid: true
						});								
						vm.newSkype = '';
						vm.addNewSkype.newSkype.$setValidity('validationFormServer', true);
						vm.addNewSkype.newSkype.$setPristine();
					}else{
						vm.addNewSkype.newSkype.$setValidity('validationFormServer', false);
						vm.validationFormServerMsg = data[1];
					}
				});
		}
		
		function save(skypeObj, arrKey){
			$http({
				method: "POST",
				url:    url + '/put',
				headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
				data: $.param(skypeObj)
			}).success(function(data){
				if(data[0] == 1){
					skypeObj.skype_old = skypeObj.skype;
					skypeObj.isValid = true;
				}else{
					skypeObj.isValid = false;
					skypeObj.validationFormServerMsg = data[1];
				}
			});
		}
		
		function del(skypeObj, arrKey){
			profileModal.isOpen = true;
			profileModal.onConfirm(function(){
				$http({
					method: "POST",
					url:    url + '/delete',
					headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
					data: $.param(skypeObj)
				}).success(function(data){
					if(data[0] == '1')
						vm.skypeList.splice(arrKey, 1);
				});
			});			
		}			
	}
})();