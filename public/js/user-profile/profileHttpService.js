(function(){
    'usr strict';

    angular
        .module('profileHttp', [])
        .factory('profileHttpService', profileHttpService);

        profileHttpService.$inject = ['$http', '$q'];

        function profileHttpService($http, $q){
            var apiUrl = '/'+ lang + '/user/api/';
            return{
                getSkypes: function(userId){
                    return this.http({
                        method: "GET",
                        url: apiUrl + 'skype/' + userId + '/get'
                    });
                },
                getEmails: function(userId){
                    return this.http({
                        method: "GET",
                        url: apiUrl + 'email/' + userId + '/get'
                    });
                },
                getPhones: function(userId){
                    return this.http({
                        method: "GET",
                        url: apiUrl + 'phone/' + userId + '/get'
                    });
                },
                http: function(config){
                    return $q(function(resolve, reject){
                        $http({
                            method: config.method,
                            url:    config.url,
                            headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
                            data: config.data
                        }).then(function successCallback(response) {
                            resolve(response);
                        }, function errorCallback(response) {
                            reject(response);
                        });
                    });
                },
                get: function(url){
                    return $q(function(resolve, reject){
                        $http.get(url).then(function(data){
                            resolve(data.data);
                        }, function(error){
                            reject(error);
                        });
                    });
                }
            };
        }
})();
