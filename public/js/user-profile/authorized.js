$(document).ready(function() {
    var authorizedHeaderActionItem = $('.authorized-header__action-item');

    // authorizedUserImgBlock
    authorizedHeaderActionItem.on('click', function(event) {
//        event.preventDefault();
        $('.authorized-header__action-hidden-area').removeClass('active');
        $(this).find('.authorized-header__action-hidden-area').toggleClass('active');
    });
    $(document).mouseup(function(e) {
        if (!authorizedHeaderActionItem.is(e.target) && authorizedHeaderActionItem.has(e.target).length === 0) {
            authorizedHeaderActionItem.find('.authorized-header__action-hidden-area').removeClass('active');
        }
    });
    // END:authorizedUserImgBlock
});


