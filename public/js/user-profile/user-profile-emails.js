(function(){
	'use strict';

	angular.module('cabinet')
	   .directive('emailEditor', emailEditordirective);

	function emailEditordirective(){
		return {
			restrict: "E",
			controllerAs: "emEd",
			controller: emailEditorController,
			templateUrl: "/user-profile-angular-fragments/userEmails.html"
		};
	}

	emailEditorController.$inject = ['$scope', '$http', 'profileModal', 'profileHttpService'];

	function emailEditorController($scope, $http, profileModal, profileHttpService){
		var vm = this;
		var userId = user_id;
		var url = userId ? '/'+ lang + '/user/api/email/'+userId : '/'+ lang +'/admin/api/email' ;
		vm.emailList = [];
		vm.newEmail = '';
		vm.validationFormServerMsg = '';

		vm.getEmails = getEmails;
		vm.addNew = addNew;
		vm.save = save;
		vm.del = del;

		//Init
		vm.getEmails();

		function getEmails(){
			if(userId){
				profileHttpService.getEmails(userId).then(function(data){
					vm.emailList = data.data;
					vm.emailList.forEach(function(item){
						item.email_old = item.email;
						item.isValid = true;
					});
				}, function(err){
					console.log(err);
				});
			}else{
				profileHttpService.get(url+'/get').then(function(data){
					vm.emailList = data;
					vm.emailList.forEach(function(item){
						item.email_old = item.email;
						item.isValid = true;
					});
				}, function(err){
					console.log(err);
				});
			}			
		}
		
		function addNew(){
			//Adding input change watcher
			if(!vm.addNewEmail.newEmail.$viewChangeListeners.length){
				vm.addNewEmail.newEmail.$viewChangeListeners.push(function(){
					vm.addNewEmail.newEmail.$setValidity('validationFormServer', true);
				});
			}

			if(vm.addNewEmail.$valid){
				$http({
					method: "POST",
					url:    url + '/post',
					headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
					data: $.param({email: vm.newEmail})
					
				}).success(function(data){
					if(data[0] == '1'){
						vm.emailList.push({
							email: vm.newEmail,
							email_old: vm.newEmail,
							isValid: true
						});
						vm.newEmail = '';
						vm.addNewEmail.newEmail.$setValidity('validationFormServer', true);
						vm.addNewEmail.newEmail.$setPristine();
					}else{
						vm.addNewEmail.newEmail.$setValidity('validationFormServer', false);
						vm.validationFormServerMsg = data[1];
					}
				});
			}
		}
		
		function save(emailObj){
			$http({
				method: "POST",
				url:    url + '/put',
				headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
				data: $.param(emailObj)
			}).success(function(data){
				if(data[0] === 1){
					emailObj.email_old = emailObj.email;
					emailObj.isValid = true;
				}else{
					emailObj.isValid = false;
					emailObj.validationFormServerMsg = data[1];
				}
			});
		}
		
		function del(emailObj, arrKey){	
			profileModal.isOpen = true;
			profileModal.onConfirm(function(){
				$http({
					method: "POST",
					url:    url + '/delete',
					headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
					data: $.param(emailObj)
				}).success(function(data){										
					if(data[0] == '1') {
						vm.emailList.splice(arrKey, 1);
					}
				});	
			});			
		}
	 }
})();