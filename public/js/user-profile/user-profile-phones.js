(function(){
	'use strict';

	angular.module('cabinet')
	   .directive("phoneEditor", phoneEditor); 


	function phoneEditor(){
		return {
			controllerAs: "phEd",
			restrict: "E",
			templateUrl: "/user-profile-angular-fragments/userPhones.html",
			controller: phoneEditorController
		};
	}

	phoneEditorController.$inject = ['$http', 'profileModal', 'profileHttpService'];

	function phoneEditorController($http, profileModal, profileHttpService){
		var vm = this;
		var userId = user_id;
		var url = '/'+ lang +'/user/api/phone/' + userId;
		vm.phoneList = [];
		vm.countriesList = [];				
		vm.newPhone = {};
		vm.validationFormServerMsg = '';

		vm.getPhones = getPhones;				
		vm.addNew = addNew;
		vm.savePhone = savePhone;
		vm.delPhone = delPhone;

		//Init
		vm.getPhones();
		
		function addNew(){
			//Adding input change watcher
			if(!vm.addNewPhoneForm.phoneInfo.$viewChangeListeners.length){
				vm.addNewPhoneForm.phoneInfo.$viewChangeListeners.push(function(){
					vm.addNewPhoneForm.phoneInfo.$setValidity('validationFormServer', true);
				});
			}
			if(vm.addNewPhoneForm.$valid){
				$http({
					method: 'POST',
					url:    url + '/post',
					headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
					data: $.param({phone: vm.newPhone.phone, country_code: vm.newPhone.country_code})							
				}).success(function(data){
					if(data[0] == 1){
						vm.phoneList.push({
							user_id  	: null,
							phone    	: vm.newPhone.phone,
							phone_old	: vm.newPhone.phone,
							country_code: vm.newPhone.country_code,
							isValid: true
						});
						vm.addNewPhoneForm.phoneInfo.$setValidity('validationFormServer', true);
						vm.addNewPhoneForm.phoneInfo.$setPristine();
					}else{
						vm.addNewPhoneForm.phoneInfo.$setValidity('validationFormServer', false);
						vm.validationFormServerMsg = data[1];
					}
				});
			}
		}
		
		function savePhone(phonInfo){
			if(!vm.phoneListForm.$valid){
				return;
			}
			$http({
				method: "POST",
				url:    url + '/put',
				headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
				data: $.param(phonInfo)
			}).success(function(data){
				if(data[0] == '1'){
					phonInfo.phone_old = phonInfo.phone;
					phonInfo.isValid = true;
				}else{
					phonInfo.isValid = false;
					phonInfo.validationFormServerMsg = data[1];
				}
			}, function(err){
			});
		}
		
		function delPhone(phonInfo){
			profileModal.isOpen = true;
			profileModal.onConfirm(function(){
				$http({
				method: "POST",
				url:    url + '/delete',
				headers: {"Content-Type" : "application/x-www-form-urlencoded;charset=utf-8"},
				data: $.param({phone_old: phonInfo.phone_old})
				}).success(function(data){						
					if(data[0] == '1'){
						var delItemINdex = vm.phoneList.indexOf(phonInfo);
						vm.phoneList.splice(delItemINdex, 1);
						console.log(data);
					}
				});	
			});					
		}

		function getPhones(){
			profileHttpService.getPhones(userId).then(function(data){
				vm.phoneList = data.data;
				vm.phoneList.forEach(function(item){
					item.phone_old = item.phone;
					item.isValid = true;
				}, function(error){
					console.log(error);
				});
			});
		}
	}
})();