(function(){
	'use strict';

	var app = angular.module('cabinet', ['ui.bootstrap.modal', 'profileHttp']);
	 

	app.directive('cabinetFragments', cabinetFragments);

	function cabinetFragments(){
		return {
			restrict: "A",
			controllerAs: "cabFragm",
			controller: cabinetFragmentsController
		};
	}
	
	cabinetFragmentsController.$inject = [];

	function cabinetFragmentsController(profileModal){
		var vm = this; 
		vm.selected = null;
		
		vm.selectFragment = selectFragment;	
		
		function selectFragment(fragment){
			vm.selected = fragment;
		}
	}

	// app.directive('profileModal', profileModal);
    //
	// profileModal.$inject = [];
    //
	// function profileModal(){
	// 	console.log('init profileModal')
	// 	return {
	// 		restrict: "A"
	// 	};
	// }

	// app.directive("emailEditor", emailEditor);
	 
	// function emailEditor(){
	// 	var controller = ['$http', '$scope', function($http, $scope){
			
	// 	}];
		
	// 	return {
	// 		restrict: "A",
	// 		controllerAs: "emEd",
	// 		controller: controller,
	// 	};
	//  }

	

})();

(function(){
	'use strict';


	angular
		.module('cabinet')
		.factory('profileModal', profileModal);

	profileModal.$inject = [];

	function profileModal(){
		var onCencelHandler;
		var onConfirmHandler;
		return{
			isOpen: false,
			onConfirm: function(callback){
				onConfirmHandler = callback;
			},
			cencel: function(){
				if(onCencelHandler){
					onCencelHandler();
				}
			},
			confirm: function(){
				if(onConfirmHandler){
					onConfirmHandler();
				}
			}
		};
	}
})();

(function(){
	'use strict';


	angular
		.module('cabinet')
		.controller('Modalctrl', modalInstanceCtrl);

	modalInstanceCtrl.$inject = ['profileModal'];
	
	function modalInstanceCtrl(profileModal){
		var vm = this;
		vm.ok = ok;
		vm.close = close;
		vm.modal = profileModal;
		vm.show = show;

		function show(){		
			profileModal.isOpen = true;
		}

		function ok(){
			profileModal.confirm();
			vm.close();
		}

		function close(){
			profileModal.isOpen = false;
		}
	}
})();
