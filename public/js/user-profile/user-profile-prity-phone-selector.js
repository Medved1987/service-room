
(function(){
	'use strict';


	angular.module('cabinet')
		.provider('ngIntlTelInput', ngIntlTelInputProvider);

	angular.module('cabinet')
		.directive('ngIntlTelInput', ngIntlTelInputDirective);

	function ngIntlTelInputProvider() {
		var me = this;
		var props = {};
		var setFn = function (obj) {
			console.log('setFn', obj);
			if (typeof obj === 'object') {
				for (var key in obj) {
					props[key] = obj[key];
				}
			}
		};
		me.set = setFn;

		me.$get = ['$log', function ($log) {
			return Object.create(me, {
				init: {
					value: function (elm) {
						if (!window.intlTelInputUtils) {
							$log.warn('intlTelInputUtils is not defined. Formatting and validation will not work.');
						}
						elm.intlTelInput(props);
					}
				},
			});
		}];
	}

	ngIntlTelInputDirective.$inject = ['ngIntlTelInput', '$log'];

	function ngIntlTelInputDirective(ngIntlTelInput, $log) {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function (scope, elm, attr, ctrl) {
					// Warning for bad directive usage.
					if ((!!attr.type && (attr.type !== 'text' && attr.type !== 'tel')) || elm[0].tagName !== 'INPUT') {
						$log.warn('ng-intl-tel-input can only be applied to a *text* or *tel* input');
						return;
					}

					// Override default country.
					if (attr.preferredCountries) {
						//ngIntlTelInput.set({preferredCountries: attr.preferredCountries.split(',')});
						// elm.intlTelInput({preferredCountries: attr.preferredCountries.split(',')});
					}

					// Initialize.
					ngIntlTelInput.init(elm);

					elm.on("countrychange", function(e, countryData) {
//						debugger
						var number = elm.intlTelInput('getNumber').replace(/[^\d]/, '');
						ctrl.$modelValue = ctrl.$modelValue || ctrl.$$rawModelValue;						
						ctrl.$modelValue.country_code = countryData.iso2;
						ctrl.$modelValue.phone = number || ctrl.$modelValue.phone;
						var isValid = elm.intlTelInput("isValidNumber");
						if(!number && ctrl.$modelValue.phone){
							//elm.intlTelInput('setNumber', ctrl.$modelValue.phone);
							return;
						}
						ctrl.$setValidity('is_valid', isValid);
						ctrl.$validate();
					});

					// Validation.
					//   ctrl.$validators.ngIntlTelInput = function (value) {
					// 	  debugger
					//     // if phone number is deleted / empty do not run phone number validation
					//     if (value || elm[0].value.length > 0) {
					//         return elm.intlTelInput("isValidNumber");
					//     } else {
					//         return true;
					//     }
					//   };

					//Set model value to valid, formatted version.
					ctrl.$parsers.push(function (value) {
						var isValid = elm.intlTelInput("isValidNumber");
						ctrl.$setValidity('is_valid', isValid);
						var resulte = {
							phone: elm.intlTelInput('getNumber').replace(/[^\d]/, ''),
							country_code: elm.intlTelInput('getSelectedCountryData').iso2,
							phone_old: ctrl.$modelValue ? ctrl.$modelValue.phone_old : ctrl.$$rawModelValue.phone_old,
							isValid: ctrl.$modelValue ? ctrl.$modelValue.isValid : ctrl.$$rawModelValue.isValid
						};
						return resulte;
					});

					// Set input value to model value and trigger evaluation.
					ctrl.$formatters.push(function (value) {
						var result;
						if (value && typeof value === "string") {
							if (value.charAt(0) !== '+') {
								result = '+' + value;
							} else {
								result = value;
							}
						}
						else if (typeof value === "object") {
							if (!value.hasOwnProperty('phone')) {
								result = '';
							} else if (value.phone.charAt(0) !== '+') {
								result = '+' + value.phone;
							} else {
								result = value.phone;
							}
						}
						elm.intlTelInput('setNumber', result);
						return result;
					});
				}
			};
		}
})();