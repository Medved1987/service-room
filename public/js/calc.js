(function() {
    Object.assign = Object.assign || function(target, source) {
        function toObject(val) {
            if (val === null || val === undefined) {
                throw new TypeError('Object.assign cannot be called with null or undefined');
            }
            return Object(val);
        }
        var from;
        var to = toObject(target);
        var symbols;

        for (var s = 1; s < arguments.length; s++) {
            from = Object(arguments[s]);

            for (var key in from) {
                if (hasOwnProperty.call(from, key)) {
                    to[key] = from[key];
                }
            }

            if (Object.getOwnPropertySymbols) {
                symbols = Object.getOwnPropertySymbols(from);
                for (var i = 0; i < symbols.length; i++) {
                    if (propIsEnumerable.call(from, symbols[i])) {
                        to[symbols[i]] = from[symbols[i]];
                    }
                }
            }
        }

        return to;
    };
})()

$(function() {
    numeral.locale('ru' === lang ? lang : 'en');
    numeral.defaultFormat('0,0.00 $');

    /*
         Already функция
    */
    $('body').on('click', '#allreadyOrder', function() {
        $("#allreadyOrder").prop("disabled", true);
        switch (menuManager.typeTrans) {
            case 'all':
                if (orderData.fromCity.item.data === 'no') {
                    $('#acFromCities').val('');
                }
                if (orderData.inCity.item.data === 'no') {
                    $('#acInCities').val('');
                }
                break;
            case 'truck':
                if (orderData.fromCity.item.data === 'no') {
                    $('#acFromCities').val('');
                }
                if (orderData.inCity.item.data === 'no') {
                    $('#acInCities').val('');
                }
                orderData.prop('freight', $('#COIFreight').prop("checked"));
                orderData.prop('accesPickup', $('#Access_Delivery').prop("checked"));
                orderData.prop('accesDelivery', $('#Access_Pickup').prop("checked"));
                break;
            case 'rail':
                orderData.prop('FCL', menuManager.railType);
                if (orderData.fromCity.item.data === 'no') {
                    $('#acFromCities').val('');
                }
                if (orderData.inCity.item.data === 'no') {
                    $('#acInCities').val('');
                }
                orderData.prop('freight', $('#COIFreight').prop("checked"));
                orderData.prop('accesPickup', $('#Access_Delivery').prop("checked"));
                orderData.prop('accesDelivery', $('#Access_Pickup').prop("checked"));
                break;
            case 'air':
                if (orderData.fLocation.item.label === 'no') {
                    $('#AirFrom').val('');
                }
                if (orderData.iLocation.item.label === 'no') {
                    $('#AirTo').val('');
                }
                break;
            case 'break-bulk':
                if (orderData.fLocation.item.label === 'no') {
                    $('#BulkPortTo').val('');
                }
                if (orderData.iLocation.item.label === 'no') {
                    $('#BulkPortFrom').val('');
                }
                break;
            case 'ocean':
                if (orderData.fLocation.item.label === 'no') {
                    $('#portFrom').val('');
                }
                if (orderData.iLocation.item.label === 'no') {
                    $('#portTo').val('');
                }
                break;
            default:
                console.log('->error:unexpect type!(get ready)');
        }


        var log_1 = $('#block_2').valid(),
            log_2 = $('#selectLoad').valid(),
            log_3 = true,
            log_4 = true,
            log_5 = $('#hazardous-form').valid();

        if ($('#ModeSelection').val() == '0' &&
            menuManager.typeTrans == 'break-bulk'
        ) {
            $('#ModeSelection + .ui-selectmenu-button').addClass('failed');
            log_3 = false;
        }

        if ($('#typeCommodity').val() == '0') {
            $('.calculator .tabs .tab-section.locations .ui-selectmenu-button').addClass('failed');
            log_4 = false;
        }

        if ($('#hazardousClassSelect').val() == '0') {
            $('#hazardousClassWrap .ui-selectmenu-button').addClass('failed');
            log_5 = false;
        }

        function scroll_to_elem(elem, speed) {
            if (document.getElementById(elem)) {
                var destination = jQuery('#' + elem).offset().top;
                jQuery("html,body").animate({ scrollTop: destination }, speed);
            }
        }

        try {
            if (log_1 == true &&
                log_2 == true &&
                log_3 == true &&
                log_4 == true &&
                log_5 == true
            ) {
                $('.calculator .shipping').css({
                    "pointer-events": 'none'
                });
                if (onlyFCL == false && menuManager.railType !== 'Car Carrier') {
                    orderData.prop('weight', toFloat($('#inpWeight').val()));
                    orderData.prop('length', toFloat($('#inpLength').val()));
                    orderData.prop('width', toFloat($('#inpWidth').val()));
                    orderData.prop('height', toFloat($('#inpHeight').val()));
                }

                function setTypeConv(type) {
                    console.log(type);
                    switch (type) {
                        case 'kg/Cm':
                            return 'kg';
                            break;
                        case 'lb/In':
                            return 'lb';
                            break;
                        default:
                            return 'kg';
                    }
                };
                orderData.prop('conv', setTypeConv($('#quantitiesSelection').val()));
                orderData.prop('commodity', $('#typeCommodity').val());
                orderData.prop('pickFromAddr', $('#pickFromAddr').prop("checked"));
                orderData.prop('pickInAddr', $('#pickInAddr').prop("checked"));
                orderData.prop('insureVal', toFloat($('#inpDeclaredVal').val()));
                orderData.prop('Hazardous', $('#Hazardous_Shipment').prop("checked"));
                orderData.prop('flatbed', $('#FlatbedCh').prop("checked"));
                orderData.prop('Insurance', $('#Add_Insurance').prop("checked"));
                if (menuManager.typeofCL === 'FCL') {
                    orderData.prop('FCL', $('#FCLSelection').val());
                }

                orderData.prop('refer', $('#RefrigeratedCh').prop("checked"));
                orderData.prop('modeSelection', $('#ModeSelection').val());

                orderData.prop('itemsParameters', getItemsParameters());
                orderData.prop('hazardousText', getHazardousText());
                orderData.prop('hazardousClass', getHazardousClass());

                orderData.prop('addressPickup', $('#addressPickup').val());
                orderData.prop('addressDelivery', $('#addressDelivery').val());
                orderData.prop('incoterms', $('#incoterms').val());

                //					postDataToServer();
                mapManager.shMap();
                console.log('Done');
                $("#responseDialog").show();
                resetResults()
                scroll_to_elem('responseDialog', 1500);
                // beginTimer(26000);
                onlyFCL = false;
                //showOfferInfo();
            } else {
                $("#allreadyOrder").prop("disabled", false);
            }
        } catch (e) {
            console.log('error:', e);
        }
    });

    $('#getEcelBtn').on('click', function(event) {
        createExcell({ data: JSON.stringify(servicesData) });
    });

//    $('#printBtn').on('click', function(event) {
//      window.print();
//              // window.print();
//              var yourDOCTYPE = "<!DOCTYPE html>"; // your doctype declaration
//              var printPreview = window.open('about:blank', 'print_preview', "resizable=yes,scrollbars=yes,status=yes");
//              var printDocument = printPreview.document;
//              printDocument.open();
//              printDocument.write(yourDOCTYPE +
//                  "<html>" +
//                  document.getElementById('responseDialog').innerHTML +
//                  "</html>");
//              printDocument.close();
//    });

    $('#printBtn').on('click', showPrintPreview);

    $('#outer-servisec-filtration-select').on("selectmenuselect", function(event, ui) {
        filtrateOuterServicesByCarrier(ui.item.value);
        filtretInnerServicesByCarrier(ui.item.value);
    });

    $('#outer-servisec-search').on('change', function() {
        filtrateOuterServices(this.value);
        filtretInnerServices(this.value);
    });

    $('.return-to-calculator>.anchor').on('click', function(eve) {
        eve.preventDefault();
        $("html,body").animate({ scrollTop: 0 }, 1500);
    });


    var mapManager = new MapManager

});

var servicesData = [];
var innerServicesDataForFiltration = [];
var outerServicesDataForFiltration = [];
var filtredServicesTerm = [];

function filtrateOuterServicesByCarrier(term) {
    var filtred = outerServicesDataForFiltration.filter(function(tariff) {
        return !term || tariff.carrier == term;
    });
    renderFiltredOuterServices(filtred);
}

function filtretInnerServicesByCarrier(carrier) {
    var filtredData = innerServicesDataForFiltration.reduce(function(prev, curent) {
        var filtred = curent.filter(function(item) {
            return !carrier || carrier == (item.carrierName || item.companyName);
        });
        if (filtred.length) {
            prev.push(filtred)
            return prev;
        }
        return prev;
    }, [])
    $('.calculator-results>.results-list ').html('');
    renderPostResult(filtredData);
}

function filtrateOuterServices(term) {
    term = term.toLowerCase();
    var filtred = outerServicesDataForFiltration.filter(function(tariff) {
        if (tariff.carrier.toLowerCase().indexOf(term) !== -1) {
            return true;
        }
        if (tariff.service && tariff.service.toLowerCase().indexOf(term) !== -1) {
            return true;
        }
        if (tariff.price.toString().toLowerCase().indexOf(term) !== -1) {
            return true;
        }
    });
    renderFiltredOuterServices(filtred);
}

function filtretInnerServices(term) {
    term = term.toLowerCase();
    var filtredData = innerServicesDataForFiltration.reduce(function(prev, curent) {
        var filtred = curent.filter(function(item) {
            if (item.carrierName && item.carrierName.toLowerCase().indexOf(term) !== -1) {
                return true;
            }
            if (item.companyName && item.companyName.toLowerCase().indexOf(term) !== -1) {
                return true;
            }
            if (item.product.toString().toLowerCase().indexOf(term) !== -1) {
                return true;
            }
        });
        if (filtred.length) {
            prev.push(filtred)
            return prev;
        }
        return prev;
    }, [])
    $('.calculator-results>.results-list ').html('');
    renderPostResult(filtredData);
}

function renderFiltredOuterServices(services) {
    var outerServicesElement = $('#outer-services');
    outerServicesElement.html('');
    services.forEach(function(tariff) {
        outerServicesElement.append(renderOuterServiceTemplate(tariff));
    });
}

function renderOuterServiceTemplate(tariff) {
    var delivery = tariff.delivery === '' ? noDelivery : tariff.delivery + ' ' + dayLocalization;
    return '<div><div class="service-cell"><span class="service-logo"><img src="' + tariff.logo + '"/></span>' +
        '<span class="service-name">' + tariff.carrier + '</span>' +
        '<span class="tariff-service">' + (tariff.service || '') + '</span>' +
        '</div>' +
        '<span class="service-value">' + numeral(tariff.price).format() + '</span>' +
        '<span class="term-delivery">' + delivery + '</span>' +
        '<span class="outer-services__btn-reserve">' + reserveBtn + '</span></div>';
}

function addFiltrationTerm(name) {
    if (filtredServicesTerm.indexOf(name) !== -1) {
        return;
    }
    filtredServicesTerm.push(name);
    var servisecSiltrationSelect = $('#outer-servisec-filtration-select')
    servisecSiltrationSelect.append('<option value="' + name + '">' + name + '</option>');
    servisecSiltrationSelect.selectmenu("refresh");
}

function resetResults() {
    $('.results-list').html('');
    $('#outer-services').html('');
    $('#outer-servisec-filtration-select').html('<option selected value="">All</option>');
    $('.outer-services__wrapper').hide();
    $('.results-list').hide();
    $('#inner-services-title').hide();
    servicesData = [];
    innerServicesDataForFiltration = [];
    outerServicesDataForFiltration = [];
    filtredServicesTerm = [];
}

function getUserInfo() {
    $.get('/' + lang + '/api/get-user-info').then(function(data) {
        if (!data.success) {
            return;
        }
        console.log(data);
        $('.user-info .user-name').html(data.user_info.fio);
        $('.user-info .user-e-mail').html(data.user_info.email.toString());
        // $('.user-info .user-skype').html()
        $('.user-info .user-phone').html(data.user_info.phone.toString());
    }, function(err) {
        console.error(err);
    });
}

function showPrintPreview() {
    // window.print();
    var yourDOCTYPE = "<!DOCTYPE html>"; // your doctype declaration
    var printPreview = window.open('about:blank', 'print_preview', "resizable=yes,scrollbars=yes,status=yes");
    var printDocument = printPreview.document;
    printDocument.open();
    printDocument.write(yourDOCTYPE +
        "<html> <style></style>" +
        document.getElementById('responseDialog').innerHTML +
        "</html>");
    printDocument.close();
    var ruls = $('style[media="print"]')[0].sheet.rules[0].cssRules;
    ruls = [].reduce.call(ruls, function(prev, curent) {
        return prev += curent.cssText;
    }, '');
    printDocument.querySelector('style').innerHTML = ruls;
}

function getServices(transpotType) {
    $.get(lang + '/api/get-service/' + transpotType)
        .then(function(res) {
            if (!res.success) {
                return;
            }
            $('#calculator-results-preloader').show();
            $('#getEcelBtn').prop("disabled", true);
            $('#printBtn').prop("disabled", true);
            var outerServicesElement = $('#outer-services');
            res.serviceList.forEach(function(item, index) {
                orderData.dataConstr('label', 'ru');
                $.post(lang + '/api/calculate', {
                    service: item.identifier,
                    data: {
                        fromCountry: orderData.fromCountry.item.code,
                        fromCity: orderData.fromCity.item.data[lang],
                        inCountry: orderData.inCountry.item.code,
                        inCity: orderData.inCity.item.data[lang],
                        commodity: orderData.commodity,
                        FCL: orderData.FCL,
                        weight: isNaN(orderData.weight) ? null : orderData.weight,
                        length: isNaN(orderData.length) ? null : orderData.length,
                        width: isNaN(orderData.width) ? null : orderData.width,
                        height: isNaN(orderData.height) ? null : orderData.height,
                        volume: isNaN(orderData.volume) ? null : orderData.volume,
                        pickFromAddr: orderData.pickFromAddr,
                        pickInAddr: orderData.pickInAddr,
                        insureVal: orderData.insureVal,
                        insurance: orderData.insurance,
                        conv: orderData.conv,
                        lang: orderData.lang,
                        calcType: menuManager.typeTrans ? menuManager.typeTrans : orderData.calcType,
                        offer_id: parseInt(orderData.offer_id),
                        rates: RU_USD,
                        itemsParameters: orderData.itemsParameters
                    }

                }).then(function(data) {
                    if (data.success && data.result.success) {
                        data.result.tariff.forEach(function(tariff) {
                            tariff.carrier = item.name;
                            tariff.logo = item.logo;
                            tariff.calc_type = menuManager.typeTrans ? menuManager.typeTrans : orderData.calcType;
                            outerServicesDataForFiltration.push(tariff);
                            servicesData.push({
                                carrier: item.name,
                                tariff: tariff.price,
                                delivery: tariff.delivery,
                                calc_type: menuManager.typeTrans ? menuManager.typeTrans : orderData.calcType,
                            });
                            addFiltrationTerm(tariff.carrier);
                            outerServicesElement.append(renderOuterServiceTemplate(tariff));
                        });
                        $('.outer-services__wrapper').show();
                        $('#outer-servisec-item-count').selectmenu("refresh");

                    }
                    if (res.serviceList.length - 1 === index) {
                        timeUp();
                        $('#calculator-results-preloader').hide();
                        $('#getEcelBtn').prop("disabled", false);
                        $('#printBtn').prop("disabled", false);
                    }
                });
            });
        }, function(error) {
            console.error(error);
        });
}

/*
функция по готовности
*/
function getReady() {
    $.ajax({
        url: urlManager.offer,
        data: orderData.dataConstr('label', 'ru')
    }).done(function(data) {
        orderData.prop('offer_id', data.offer_id);
        getServices(menuManager.typeTrans);
        postDataToServer().then(function(data) {
                innerServicesDataForFiltration = data;
                renderPostResult(data);
                return postToCompanyTartiff();
            })
            .then(function(data) {
                innerServicesDataForFiltration = innerServicesDataForFiltration.concat(data);
                innerServicesDataForFiltration.forEach(function(items) {
                    for (var key in items) {
                        servicesData.push({
                            carrier: items[key].carrierName || items[key].companyName,
                            tariff: items[key].total_price,
                            delivery: '',
                            calc_type: menuManager.typeTrans ? menuManager.typeTrans : orderData.calcType
                        });
                    }
                });

                renderPostResult(data);
            }, function(error) {
                console.error(error);
            });
        showOfferInfo(orderData);

    });
}

function postDataToServer() { //admin/api/ffservice
    return $.post(urlManager.ffservice, orderData.dataConstr('label', 'ru'));
}

function postToCompanyTartiff() {
    return $.post(urlManager.company_tariff, orderData.dataConstr('label', 'ru'))
}

function createExcell(params) {
    var form = document.createElement("form");
    form.setAttribute("method", 'post');
    form.setAttribute("action", '/' + lang + '/api/generate-excel');

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}


function MapManager() {
    if (!google) {
        return;
    }
    var map_1;
    var map_2;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService();
    this.markers = [];
    this.init = function() {
        var latlng = new google.maps.LatLng(57.0442, 9.9116);
        var settings = {
            zoom: 1,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU },
            navigationControl: true,
            navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL },
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map_1 = new google.maps.Map(document.getElementById("Gmap_1"), settings);
        map_2 = new google.maps.Map(document.getElementById("Gmap_2"), settings);
        $("#Gmap_1").hide();
        $("#Gmap_2").hide();
    };
    this.init();
    this.marker = function(map, lat, lon, titl) {
        var Pos = new google.maps.LatLng(lat, lon);
        this.markers[this.markers.length] = new google.maps.Marker({
            position: Pos,
            map: map,
            title: titl
        });
        map.setCenter(Pos);
        map.setZoom(11);
    };
    this.clearMarkers = function() {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers.length = 0;
    };

    this.shMap = function() {

        this.clearMarkers();
        $("#Gmap_1").hide();
        $("#Gmap_2").hide();
        directionsDisplay.setDirections({ routes: [] });

        function setDirection() {
            if (point_or != undefined && point_des != undefined) {
                var request = {
                    origin: new google.maps.LatLng(point_or.lat(), point_or.lng()),
                    destination: new google.maps.LatLng(point_des.lat(), point_des.lng()),
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                };
                directionsService.route(request, function(response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        var dist = response.routes[0].legs[0].distance.value,
                            tTravel = response.routes[0].legs[0].duration.value;
                        console.log(dist);
                        orderData.prop('distance', dist);
                        orderData.prop('time_traveled', tTravel);
                        directionsDisplay.setDirections(response);
                    }
                    getReady();
                });
                directionsDisplay.setMap(map_1);
            }
        }
        switch (menuManager.typeTrans) {
            case 'all':
                function allSetDirection() {
                    $("#Gmap_1").show();
                    if (point_or != undefined && point_des != undefined) {
                        var request = {
                            origin: new google.maps.LatLng(point_or.lat(), point_or.lng()),
                            destination: new google.maps.LatLng(point_des.lat(), point_des.lng()),
                            travelMode: google.maps.DirectionsTravelMode.DRIVING
                        };
                        directionsService.route(request, function(response, status) {
                            console.log('status => ' + status);
                            if (status == google.maps.DirectionsStatus.OK) {
                                if (response.routes !== undefined) {
                                    var dist = response.routes[0].legs[0].distance.value,
                                        tTravel = response.routes[0].legs[0].duration.value;
                                    console.log(dist);
                                    orderData.prop('distance', dist);
                                    orderData.prop('time_traveled', tTravel);
                                    directionsDisplay.setDirections(response);
                                }
                            }
                            getReady();
                        });
                        directionsDisplay.setMap(map_1);
                        $("#Gmap_1").hide();
                    }
                }
                var that = this,
                    label = orderData.fromCity.item.data.en,
                    point_or,
                    point_des;
                new google.maps.Geocoder().geocode({ 'address': label }, function(results, status) {
                    point_or = Object.create(results[0].geometry.location);
                    allSetDirection();
                });
                label = orderData.inCity.item.data.en;
                new google.maps.Geocoder().geocode({ 'address': label }, function(results, status) {
                    point_des = Object.create(results[0].geometry.location);
                    allSetDirection();
                });
                break;
            case 'ocean':
                $("#Gmap_1").show();
                $("#Gmap_2").show();
                google.maps.event.trigger(map_1, 'resize');
                google.maps.event.trigger(map_2, 'resize');
                var lat,
                    lon,
                    label;
                lat = orderData.fLocation.item.latitude;
                lon = orderData.fLocation.item.longitude;
                label = orderData.fLocation.item.label;
                this.marker(map_1, lat, lon, label);
                lat = orderData.iLocation.item.latitude;
                lon = orderData.iLocation.item.longitude;
                label = orderData.iLocation.item.label;
                this.marker(map_2, lat, lon, label);
                getReady();
                break;
            case 'rail':
                $("#Gmap_1").show();
                google.maps.event.trigger(map_1, 'resize');

                var region = '';
                if (orderData.fcode !== undefined && orderData.fcode !== '') {
                    region = ',' + orderData.fcode;
                }

                var that = this,
                    label = orderData.fromCity.item.data['en'] + region,
                    point_or,
                    point_des;
                new google.maps.Geocoder().geocode({ 'address': label }, function(results, status) {
                    point_or = Object.create(results[0].geometry.location);
                    setDirection();
                });

                region = '';
                if (orderData.icode !== undefined && orderData.icode !== '') {
                    region = ',' + orderData.icode;
                }

                label = orderData.inCity.item.data['en'] + region;
                new google.maps.Geocoder().geocode({ 'address': label }, function(results, status) {
                    point_des = Object.create(results[0].geometry.location);
                    setDirection();
                });
                break;
            case 'truck':
                function setDirection() {
                    if (point_or != undefined && point_des != undefined) {
                        var request = {
                            origin: new google.maps.LatLng(point_or.lat(), point_or.lng()),
                            destination: new google.maps.LatLng(point_des.lat(), point_des.lng()),
                            travelMode: google.maps.DirectionsTravelMode.DRIVING
                        };
                        directionsService.route(request, function(response, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                var dist = response.routes[0].legs[0].distance.value,
                                    tTravel = response.routes[0].legs[0].duration.value;
                                console.log(dist);
                                orderData.prop('distance', dist);
                                orderData.prop('time_traveled', tTravel);
                                directionsDisplay.setDirections(response);
                            }
                            getReady();
                        });
                        directionsDisplay.setMap(map_1);
                    }
                }
                $("#Gmap_1").show();
                google.maps.event.trigger(map_1, 'resize');
                var that = this,
                    label = orderData.fromCity.item.data.ru,
                    point_or,
                    point_des;
                new google.maps.Geocoder().geocode({ 'address': label }, function(results, status) {
                    point_or = Object.create(results[0].geometry.location);
                    setDirection();
                });
                label = orderData.inCity.item.data.ru;
                new google.maps.Geocoder().geocode({ 'address': label }, function(results, status) {
                    point_des = Object.create(results[0].geometry.location);
                    setDirection();
                });

                break;
            case 'air':
                $("#Gmap_1").show();
                $("#Gmap_2").show();
                google.maps.event.trigger(map_1, 'resize');
                google.maps.event.trigger(map_2, 'resize');
                var that = this,
                    label = orderData.fLocation.item.label;
                if (label.indexOf('/') != -1) {
                    label = label.substr(label.indexOf('/'));
                }

                new google.maps.Geocoder().geocode({ 'address': label }, function(results, status) {
                    that.markers[that.markers.length] = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map_1,
                        title: label
                    });
                    map_1.setCenter(results[0].geometry.location);
                    map_1.setZoom(10);
                });

                label = orderData.iLocation.item.label;
                if (label.indexOf('/') != -1) {
                    label = label.substr(label.indexOf('/'));
                }
                new google.maps.Geocoder().geocode({ 'address': label }, function(results, status) {
                    that.markers[that.markers.length] = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map_2,
                        title: label
                    });
                    map_2.setCenter(results[0].geometry.location);
                    map_2.setZoom(10);
                });
                getReady();
                break;
            case 'break-bulk':
                $("#Gmap_1").show();
                $("#Gmap_2").show();
                google.maps.event.trigger(map_1, 'resize');
                google.maps.event.trigger(map_2, 'resize');
                var lat,
                    lon,
                    label;
                lat = orderData.fLocation.item.latitude;
                lon = orderData.fLocation.item.longitude;
                label = orderData.fLocation.item.label;
                this.marker(map_1, lat, lon, label);
                lat = orderData.iLocation.item.latitude;
                lon = orderData.iLocation.item.longitude;
                label = orderData.iLocation.item.label;
                this.marker(map_2, lat, lon, label);
                getReady();
                break;
            default:
                errors.add('-> error: unknown type of calculator! ( ' + type + ' )');
        }
    }
};

function restructor(str, split, shift) {
    shift = shift || 0;
    if (str.indexOf(split) != -1) {
        str = str.substr(0, str.indexOf(split) + shift);
    }
    return str;
}

function trimm(str) {
    str = str.split(' ');
    var returnStr = '';

    if (str.length > 1) {
        for (var temp in str) {
            returnStr += str[temp];
        }
    } else {
        returnStr = str[0];
    }

    return returnStr;
}

function checker(str, split, check) {
    var arrSplit = [],
        toProc = false,
        stopEl = null,
        retStr = '';

    if (str.indexOf(split) != -1) {
        arrSplit = str.split(split);

        for (var el in arrSplit) {
            var elUp = arrSplit[el].toUpperCase(),
                search = check.toUpperCase();
            if (elUp.indexOf(search) != -1) {
                toProc = true;
                stopEl = el;
            }
        }

        if (toProc == true) {
            for (var el in arrSplit) {

                if (el != stopEl) {
                    if (el == 0) {
                        retStr = arrSplit[el];
                    } else {
                        retStr = retStr + split + arrSplit[el];
                    }

                    retStr = $.trim(retStr);
                } else {
                    return retStr;
                }

            }
        } else {
            retStr = str;
        }
        return retStr;
    }
    return str;
}

//конструктор массивов
function arrConstructor(inputArr, outputArr, labelCol, dataCol, cFunc) {
    if (inputArr == 'undefined' || outputArr == 'undefined') {
        errors.add('-> error: data are incomplete!', inputArr, outputArr);
    }

    if (typeof(labelCol) == 'undefined' ||
        typeof(dataCol) == 'undefined'
    ) {
        for (var i = 0; i < inputArr.length; i += 1) {
            outputArr[i] = inputArr[i];
        }
    } else {
        for (var i = 0; i < inputArr.length; i += 1) {
            if (typeof(cFunc) !== 'function') {
                outputArr[i] = {
                    'value': inputArr[i][labelCol],
                    'label': inputArr[i][labelCol],
                    'data': inputArr[i][dataCol]
                };
            } else {
                outputArr[i] = {
                    'value': cFunc(inputArr[i][labelCol]),
                    'label': cFunc(inputArr[i][labelCol]),
                    'data': cFunc(inputArr[i][dataCol])
                };
            }
        }
    }
    outputArr.length = inputArr.length;
}


function toFloat(price) {
    if (typeof(price) == 'string') {
        price = trimm(price);
    }
    if (price.indexOf(",") !== -1) {
        price = price.replace(",", ".");
    }
    price = parseFloat(price);
    return price;
}

function getItemsParameters() {
    var containers = $('.LCLfields-wrp .selectLoadFields');
    var res = [];
    containers.each(function(item) {
        var indexPref = item === 0 ? '' : item;
        var $this = $(this);
        res.push({
            'inpCount': $this.find('input#inpCount' + indexPref).val(),
            'inpWeight': $this.find('input#inpWeight' + indexPref).val(),
            'inpWidth': $this.find('input#inpWidth' + indexPref).val(),
            'inpLength': $this.find('input#inpLength' + indexPref).val(),
            'inpHeight': $this.find('input#inpHeight' + indexPref).val(),
            'cargoParticular': getCargoParticular($this),
            'hazardus': getHazardous($this)
        });
    });
    return res;
}

function getHazardousText() {
    return $('#hazardousText').val()
}

function getHazardousClass() {
    return $("#hazardousClassSelect").val();
}

function getCargoParticular(cargoParameters) {
    var cargoParticular = {};
    cargoParameters.find('.drop-particular input[type="checkbox"]').each(function(index) {
        if (this.checked) {
            cargoParticular[this.dataset['impCode']] = this.checked;
        }
    });
    return cargoParticular;
}

function getHazardous(cargoParameters) {
    var hazardous = {
        'un_number': cargoParameters.find('.hazardousCode').val(),
        'hazardous_class': cargoParameters.find('.hazardousClassListSelect').val()
    };
    return hazardous;
}



function getPrice(obj) {
    var price;
    switch (lang) {
        case 'ru':
            price = obj['total_price_rub'];
            break;
        case 'en':
            price = obj['total_price_usd'];
            break;
    }
    return price
}

function hadlerLocation(location) {
    var resData = [],
        locationArr = [],
        befoCity = null,
        befoCountry = null,
        city = null,
        country = null;

    if (typeof(location) !== 'string') {
        return false;
    }

    locationArr = location.split(',');
    befoCity = locationArr[0];
    befoCountry = locationArr[locationArr.length - 1];

    city = restructor(befoCity, '/');
    city = restructor(city, '(');
    city = restructor(city, 'City');
    city = restructor(city, ' - Metropolitan');
    city = restructor(city, ' - Столица');
    city = restructor(city, '-Сити');
    city = checker(city, '-', 'Airport');
    city = checker(city, '-', 'аэропорт');
    city = restructor(city, ' - ');

    country = restructor(befoCountry, '(');

    resData['city'] = trimm(city);
    resData['country'] = trimm(country);
    return resData;
}

function qCities(country, lastArr, q) {
    if (country.item.code == 'US' && menuManager.typeTrans == 'rail') {
        $.ajax({
            type: 'GET',
            url: urlManager.railCity,
            data: { "code": country.item.code, "city": q, "lang": lang },
            success: function(data) {
                try {
                    data = JSON.parse(data);

                    for (var i in data) {
                        data[i]['en'] = data[i]['city'];
                        data[i]['ru'] = data[i]['city_ru'];

                    }
                    trConstructor(data, lastArr);
                } catch (e) {
                    console.log('data:', data, ' error:', e);
                }
            }
        });
    } else {
        $.ajax({
            type: 'POST',
            url: urlManager.qCities,
            data: { "code": country.item.code, "city": q },
            success: function(data) {
                try {
                    console.log('data:', data);
                    trConstructor(data.city, lastArr);
                } catch (e) {
                    console.log('data:', data, ' error:', e);
                }
            }
        });
    }
}

function trConstructor(arr, lastArr) {
    var len = arr.length,
        tempArr = [];
    for (var i = 0; i < len; i += 1) {
        tempArr[i] = {};
        tempArr[i]["data"] = {};
        tempArr[i]["data"]['en'] = arr[i]['en'];
        tempArr[i]["data"]['ru'] = arr[i]['ru'];
        tempArr[i]["label"] = arr[i][lang];

        if (arr[i]['code'] !== undefined &&
            arr[i]['post_code'] !== undefined) {
            tempArr[i]["data"]["post_code"] = arr[i]['post_code'];
            tempArr[i]["data"]["code"] = arr[i]['code'];
            tempArr[i]["label"] = arr[i]['post_code'] + '|' + arr[i][lang] + '|' + arr[i]['code'];
        }
    }
    console.log('->Cities array:', tempArr);
    arrConstructor(tempArr, lastArr, 'label', 'data');
    switch (lastArr) {
        case arrCitiesFrom:
            loadManager.citiesData_f = true;
            $('#acFromCities').prop('readonly', false);
            $('#acFromCities').autocomplete("search", $('#acFromCities').val());
            break;
        case arrCitiesTo:
            loadManager.citiesData_d = true;
            $('#acInCities').prop('readonly', false);
            $('#acInCities').autocomplete("search", $('#acInCities').val());
            break;
    }
}

function searchFCities(lst) {
    loadManager.lastFn;
    if (typeof(lst) === 'string') {
        loadManager.lastFn = lst;
    }
    valLen = $('#acFromCities').val().length;
    if (valLen === 3) {
        var val = $('#acFromCities').val();
        if (loadManager.lastFn !== val) {
            $('#acFromCities').prop('readonly', true);
            loadManager.citiesData_f = false;
            loadManager.lastFn = val;
            qCities(orderData.fromCountry, arrCitiesFrom, val);
            if ($('#fCityLoadImg').length === 0) {
                $('#fCitiesWrapper').append('<img id="fCityLoadImg" class="spin-form-loader" src="/img/spin-form-loader.gif" alt="spin-form-loader" />');
            }
        } else {
            loadManager.citiesData_f = true;
        }
    }
    if (valLen < 3) {
        loadManager.citiesData_f = false;
    }
}

function searchICities(lst) {
    loadManager.lastIn;
    if (typeof(lst) === 'string') {
        loadManager.lastIn = lst;
    }
    valLen = $('#acInCities').val().length;
    if (valLen === 3) {
        var val = $('#acInCities').val();
        if (loadManager.lastIn !== val) {
            $('#acInCities').prop('readonly', true);
            loadManager.citiesData_d = false;
            loadManager.lastIn = val;
            qCities(orderData.inCountry, arrCitiesTo, val);
            if ($('#iCityLoadImg').length === 0) {
                $('#iCitiesWrapper').append('<img id="iCityLoadImg" class="spin-form-loader" src="/img/spin-form-loader.gif" alt="spin-form-loader" />');
            }
        } else {
            loadManager.citiesData_d = true;
        }
    }
    if (valLen < 3) {
        loadManager.citiesData_d = false;
    }
}

//Last offer info
function showOfferInfo(offerInfo) {
    if (offerInfo) {
        renderExchangeOffer(offerInfo);
        return;
    }
    $.get('/ru/acex/service/getoffer', function(data) {
        console.log('showOfferInfo', offerInfo);
        renderExchangeOffer(offerInfo);
    });
}

function arrayLangSetter(inpArr, col_ru, col_en) {
    var len = 0,
        n = 0,
        tempArrayLocation = [];
    Object.assign = Object.assign || function(target, source) {
        function toObject(val) {
            if (val === null || val === undefined) {
                throw new TypeError('Object.assign cannot be called with null or undefined');
            }
            return Object(val);
        }
        var from;
        var to = toObject(target);
        var symbols;

        for (var s = 1; s < arguments.length; s++) {
            from = Object(arguments[s]);

            for (var key in from) {
                if (hasOwnProperty.call(from, key)) {
                    to[key] = from[key];
                }
            }

            if (Object.getOwnPropertySymbols) {
                symbols = Object.getOwnPropertySymbols(from);
                for (var i = 0; i < symbols.length; i++) {
                    if (propIsEnumerable.call(from, symbols[i])) {
                        to[symbols[i]] = from[symbols[i]];
                    }
                }
            }
        }

        return to;
    };
    for (var i = 0, len = inpArr.length; i < len; i += 1) {
        switch (lang) {
            case 'ru':
                if (inpArr[i][col_ru] !== undefined && inpArr[i][col_ru] !== null) {
                    tempArrayLocation[n] = {};
                    Object.assign(tempArrayLocation[n], inpArr[i]);
                    tempArrayLocation[n]['label'] = inpArr[i][col_ru];
                    tempArrayLocation[n]['en'] = inpArr[i][col_en];

                } else {
                    tempArrayLocation[n] = {};
                    Object.assign(tempArrayLocation[n], inpArr[i]);
                    tempArrayLocation[n]['label'] = inpArr[i][col_en];
                    tempArrayLocation[n]['en'] = inpArr[i][col_en];

                }
                n += 1;
                break;
            case 'en':
                if (inpArr[i][col_en] !== undefined && inpArr[i][col_en] !== null) {
                    tempArrayLocation[n] = {};
                    Object.assign(tempArrayLocation[n], inpArr[i]);
                    tempArrayLocation[n]['label'] = inpArr[i][col_en];
                    tempArrayLocation[n]['en'] = inpArr[i][col_en];
                    n += 1;
                }
                break;
            default:
                break;
        }

    }
    for (var i = 0; i < tempArrayLocation.length; i += 1) {
        inpArr[i] = tempArrayLocation[i];
    }
    inpArr.length = Object.keys(tempArrayLocation).length;
}
