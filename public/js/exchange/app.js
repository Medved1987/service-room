(function() {
    'use strict';
    angular.module('exchange', []);

    angular.module('exchange').directive('exchangeCategory', exchangeCategory);
    angular.module('exchange').directive('selectMenu', selectMenu);
    angular.module('exchange').directive('autocomplete', autocomplete);

    angular.module('exchange').controller('exchangeController', ExchangeController);
    ExchangeController.$inject = ['$http', '$scope'];

    /*****Directives*****/

    function exchangeCategory() {
        return {
            restrict: 'A',
            scope: {
                category: '=exchangeCategory'
            },
            link: function link(scope, element, attrs, controller, transcludeFn) {
                // scope.$watch('category', function(a, b) {
                //     Array.prototype.forEach.call(element.children(), function(item) {
                //         item.style.display = '';
                //     });
                //     if (scope.category.toLowerCase() == 'all') {
                //         return;
                //     }
                //     Array.prototype.forEach.call(element.children(), function(item) {
                //         var elementGroup = item.dataset.transportGroup;
                //         if (elementGroup.indexOf(scope.category.toLowerCase().replace(' ', '_')) == -1) {
                //             item.style.display = 'none';
                //         }
                //     });
                // })
            }
        }
    }

    function selectMenu() {
        return {
            restrict: 'E',
            scope: {
                pageCountChanged: '&'
            },
            compile: function() {},
            link: function(scope, element, attrs, controller, transcludeFn) {
                if (!$) {
                    console.error('"$" is "undefined"');
                    return;
                }
                $(element).selectmenu({
                    change: function(event, ui) {
                        scope.pageCountChanged(event.target.value);
                    }
                });
            }
        }
    }

    function autocomplete() {
        return {
            restrict: 'E',
            scope: {
                source: '<',
                onSelect: '$'
            },
            compile: function() {},
            link: function(scope, element, attrs, controller, transcludeFn) {
                if (!$) {
                    console.error('"$" is "undefined"');
                    return;
                }

                $(element).autocomplete({
                    source: scope.source,
                    select: function(e, ui) {
                        scope.onSelect(ui.item.value)
                    }
                });
            }
        }
    }

    /*****Controllers*****/

    function ExchangeController(http, $scope) {
        var vm = this;
        var exchange = [];
        var types = ['all', 'ocean', 'rail', 'truck', 'air'];
        var selectedGroup = 'all';
        var fromCountryCode = '';
        var inCountryCode = '';

        vm.activedGroup = 'All';
        vm.showAllCategories = false;
        vm.sortingBy = {
            name: 'date',
            reverse: true
        }
        vm.exchange = [];
        vm.fromFilters = {
            countries: [],
            cities: []
        };
        vm.toFilters = {
            countries: [],
            cities: []
        };
        vm.setedDestFilters = {
            from_country: '',
            from_city: '',
            in_country: '',
            in_city: ''
        };
        vm.setedCatagoryFilters = {
            commodities: [],
            types: []
        }


        /*****Pagination*****/
        vm.carentPage = 1;
        vm.itemPerPage = 10;
        vm.pages = 1;

        vm.nextPage = function(event) {
            event.preventDefault();
            if (vm.carentPage == vm.pages) { return; }
            vm.carentPage += 1;
            getStartItemIndex();
        }
        vm.prevPage = function(event) {
            event.preventDefault();
            if (vm.carentPage == 1) { return; }
            vm.carentPage -= 1;
            getStartItemIndex();
        }
        vm.pageRange = function() {
            var pages = new Array(vm.pages);
            return pages;
        }
        vm.setPage = function(event, pageNumber) {
            event.preventDefault();
            vm.carentPage = pageNumber;
            getStartItemIndex();
        }

        /**
         *  This is trable of using jQuery UI whit AngularJs
         */
        /*****Select events handling*****/
        $('#pages').on('selectmenuchange', function(event, ui) {
            vm.itemPerPage = parseInt(event.target.value);
            vm.pages = Math.ceil(vm.exchange.length / vm.itemPerPage);
            $scope.$apply();
        });

        $("#origin-city").autocomplete({
            select: function(e, ui) {
                vm.setedDestFilters.from_city = ui.item.value;
                $scope.$apply(function() {
                    // filtreteByProparty();
                    filtrete();
                });
            }
        });

        $("#destination-city").autocomplete({
            select: function(e, ui) {
                vm.setedDestFilters.in_city = ui.item.value;
                $scope.$apply(function() {
                    // filtreteByProparty();
                    filtrete();
                });
            }
        });

        $("#origin-country").autocomplete({
            source: JSON.parse(countries),
            select: function(e, ui) {
                vm.setedDestFilters.from_country = ui.item.value;
                fromCountryCode = ui.item.code;
                $scope.$apply(function() {
                    // filtreteByProparty();
                    filtrete();
                });
            }
        });

        $("#destination-country").autocomplete({
            source: JSON.parse(countries),
            select: function(e, ui) {
                vm.setedDestFilters.in_country = ui.item.value;
                inCountryCode = ui.item.code;
                $scope.$apply(function() {
                    // filtreteByProparty();
                    filtrete();
                });
            }
        });

        $("#destination-country").autocomplete({
            source: JSON.parse(countries),
            select: function(e, ui) {
                vm.setedDestFilters.in_country = ui.item.value;
                inCountryCode = ui.item.code;
                $scope.$apply(function() {
                    // filtreteByProparty();
                    filtrete();
                });
            }
        });



        //Sending get request to server for data
        loadInfo();

        /*****git Public functions*****/

        /**
         * Filtration by cargo activatGroup
         */
        vm.activatGroup = function(group) {
            vm.activedGroup = group;
            filtrete();
            // filtrationByCategory();
        }

        /**
         * Sets sorting parameters
         */
        vm.setSortParameter = function(propertyName) {
            if (vm.sortingBy.name == propertyName) {
                vm.sortingBy.reverse = !vm.sortingBy.reverse;
                return;
            }
            vm.sortingBy.name = propertyName;
            vm.sortingBy.reverse = false;
        }

        vm.changeCategoryFilter = function(event) {
            var commodity = event.target.dataset.commodity;
            if (event.target.checked) {
                vm.setedCatagoryFilters.commodities.push(commodity);
            } else {
                var index = vm.setedCatagoryFilters.commodities.indexOf(commodity);
                vm.setedCatagoryFilters.commodities.splice(index, 1);
            }
            // filtrationByCategory();
            filtrete();
        }

        vm.changeFilter = function(val) {
            console.log('changeFilter', val)
        }

        vm.chackInput = function(eve, name) {
            var value = eve.target.value;
            if (!value.length) {
                vm.setedDestFilters[name] = '';
                // filtreteByProparty();
                filtrete();
            }
            if (name == 'from_city' || name == 'in_city') {
                var code, inputElement;

                if (name == 'from_city') {
                    code = fromCountryCode;
                    inputElement = $("#origin-city");
                } else {
                    code = inCountryCode;
                    inputElement = $("#destination-city");
                }

                getCities({ "code": code, "city": value, "lang": lang })
                    .then(function(data) {
                        var cities = data.city.map(function(city) {
                            return {
                                label: city[lang],
                                value: city[lang].toLowerCase()
                            }
                        });
                        inputElement.autocomplete({
                            source: cities
                        });
                    });
            }
        }

        /*****Privet functions*****/

        function getStartItemIndex() {
            vm.startItemIndex = (vm.itemPerPage * vm.carentPage) - vm.itemPerPage;
            // var endItemIndex = (vm.itemPerPage * vm.carentPage);
        }

        /**
         * Doing Ajax get request to server
         */
        function loadInfo() {
            http.get('/' + lang + '/api/exchange/offers')
                .then(function(res) {
                    exchange = res.data.slice();
                    vm.exchange = res.data;
                    vm.pages = Math.ceil(vm.exchange.length / vm.itemPerPage);
                    vm.fromFilters.countries = getFilterItemsFromArray(vm.exchange, 'from_country');
                    vm.fromFilters.cities = getFilterItemsFromArray(vm.exchange, 'from_city');
                    vm.toFilters.countries = getFilterItemsFromArray(vm.exchange, 'in_country');
                    vm.toFilters.cities = getFilterItemsFromArray(vm.exchange, 'in_city');
                }, function(error) {
                    console.error(error);
                });
        }

        function getCities(params) {
            return http
                .post('/' + lang + '/acex/Api/getCity', params)
                .then(function(respons) {
                    return respons.data;
                });
        }

        function getFilterItemsFromArray(arr, termName) {
            return arr.reduce(function(prev, curent) {
                var filtretinValue = curent[termName];
                if (prev.indexOf(filtretinValue) < 0) {
                    prev.push(filtretinValue);
                }
                return prev;
            }, []);
        }

        function getFilterItemsFromObj(obj, termName) {
            return Object.keys(obj).reduce(function(prev, index) {
                var filtretinValue = obj[index][termName];
                if (prev.indexOf(filtretinValue) < 0) {
                    prev.push(filtretinValue);
                }
                return prev;
            }, []);
        }

        function filtrete() {
            vm.exchange = exchange.slice();
            var processedGroupName = vm.activedGroup.trim().replace(' ', '-').toLowerCase();;
            if (processedGroupName !== 'all') {
                vm.exchange = vm.exchange.filter(function(item) {
                    return item.calc_type == processedGroupName;
                });
            }
            filtreteByProparty();
            filtrationByCategory()
        }

        function filtreteByProparty() {
            vm.exchange = vm.exchange.filter(function(item) {
                var fromCountyIsMatch = false,
                    fromCityIsMacth = false,
                    inCountyIsMatch = false,
                    inCityIsMacth = false;

                if (!vm.setedDestFilters.from_country) {
                    fromCountyIsMatch = true;
                } else {
                    fromCountyIsMatch = item.from_country.toLowerCase() == vm.setedDestFilters.from_country.toLowerCase();
                }

                if (!vm.setedDestFilters.from_city) {
                    fromCityIsMacth = true;
                } else {
                    fromCityIsMacth = item.from_city.toLowerCase() == vm.setedDestFilters.from_city.toLowerCase();
                }

                if (!vm.setedDestFilters.in_country) {
                    inCountyIsMatch = true;
                } else {
                    inCountyIsMatch = item.in_country.toLowerCase() == vm.setedDestFilters.in_country.toLowerCase();
                }

                if (!vm.setedDestFilters.in_city) {
                    inCityIsMacth = true;
                } else {
                    inCityIsMacth = item.in_city.toLowerCase() == vm.setedDestFilters.in_city.toLowerCase();
                }

                return fromCountyIsMatch && fromCityIsMacth && inCountyIsMatch && inCityIsMacth;
            });
            vm.pages = Math.ceil(vm.exchange.length / vm.itemPerPage);
        }

        function filtrationByCategory() {
            if (!vm.setedCatagoryFilters.commodities.length) {
                // vm.exchange = exchange.slice();
            } else {
                vm.exchange = vm.exchange.filter(function(item) {
                    var isMatch = false;
                    vm.setedCatagoryFilters.commodities.forEach(function(commodity) {

                    });
                    for (var index = 0; index < vm.setedCatagoryFilters.commodities.length; index++) {
                        if (vm.setedCatagoryFilters.commodities[index] === item.commodity) {
                            isMatch = true;
                            break;
                        }
                    }
                    return isMatch;
                });
            }

            vm.pages = Math.ceil(vm.exchange.length / vm.itemPerPage);
        }
    }
})()