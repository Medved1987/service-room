function setChecked() {
    $('input:checked').each(function (value, obj) {
        var $label = $(obj).parent();
        $label.addClass("checked");
    });
}

$(function () {
    var href;
    /* check previously checked transport types */
    setChecked();
    /* activate tooltip */
    $('[data-toggle="tooltip"]').tooltip({
        delay: 200
    });

    /* удаление документа */
    $('.document-remove-button').on('click', function (event) {
        event.preventDefault();
        href = $(this).data('href');
    });

    $('.transport-type input[type="checkbox"]').on('change', function () {
        if ($(this).prop('checked')) {
                $(this).parent().addClass('checked');
            $(this).parent().animate({'backgroundColor': '#62BBB5'}, 100);
        } else {
                $(this).parent().removeClass('checked');
            $(this).parent().animate({'backgroundColor': '#FFFAF0'}, 100);
        }
    });

    $('#confirm-delete').on('click', function () {
        event.preventDefault();
        $.get(href, function (data) {
            if (data.success) {
                $("#document" + data.document_id).fadeOut();
            } else {
                alert(data.message);
            }
        });
    });
});

