var url = '';
var impOptions;
var iataOptions;

function renderRemoveButton(href) {
    return '<div class="text-right"><a class="btn btn-danger extra-charge-remove" href="' + href
        + '"><span class="glyphicon glyphicon-remove"></span></a></div>';
}

function renderFieldsetRemoveButton(fieldset) {
    return '<fieldset><div class="text-right"><button class="btn btn-danger fieldset-remove" type="button" onclick="removeFieldset(this)"><span class="glyphicon glyphicon-remove"></span></button></div>';
}

function addFields(fieldset) {
    var currentCount = $(fieldset + ' > fieldset').length;
    var template = $(fieldset + ' > span').data('template');
    template = template.replace(/__index__/g, currentCount);

    var button = renderFieldsetRemoveButton(fieldset);
    template = template.replace('<fieldset>', button);
    $(fieldset).append(template);
    applyAutocomplete();

    return false;
}

function getImpOptions() {
    impOptions = $.map(impCodeList, function (data) {
        if ('ru' === lang) {
            return {
                value: '(' + data.imp_code + ') ' + data.label_ru,
                data: data.imp_code
            }
        } else {
            return {
                value: '(' + data.imp_code + ') ' + data.label_en,
                data: data.imp_code
            }
        }
    });
}

function getIataOptions() {
    iataOptions = $.map(iataList, function (data) {
        if ('ru' === lang) {
            var value = '(' + data.iata + ') ';

            if (!data.name_rus) {
                value += data.name_eng + ', ';
            } else {
                value += data.name_rus + ', ';
            }

            if (!data.city_rus) {
                value += data.city_eng + ' ';
            } else {
                value += data.city_rus + ' ';
            }
            value += '(' + data.iso_code + ')';
            return {
                value: value,
                data: data.iata
            }
        } else {
            return {
                value: '(' + data.iata + ') ' + data.name_eng + ', ' + data.city_eng + ' ' + '(' + data.iso_code + ')',
                data: data.iata
            }
        }
    });
}

function applyAutocomplete() {

    if (undefined === impOptions && undefined !== impCodeList) {
        getImpOptions();
    }

    if (undefined === iataOptions && undefined !== iataList) {
        getIataOptions();
    }

    $('input.imp-code').devbridgeAutocomplete({
        lookup: impOptions,
        onSelect: function (suggestion) {
            $(this).val(suggestion.data);
        }
    });

    $('input.iata-code').devbridgeAutocomplete({
        lookup: iataOptions,
        minChars: 2,
        onSelect: function (suggestion) {
            $(this).val(suggestion.data);
        }
    });
}

function removeFields(fieldset) {
    var lastChild = $(fieldset + ' > fieldset').last();
    $(lastChild).remove();
    return false;
}

function removeFieldset(element) {
    $(element).parent().parent().fadeOut(function () {
        $(this).remove();
    });
}

$(function () {
    if (undefined !== impCodeList) {
        getImpOptions();
    }

    if (undefined !== iataList) {
        getIataOptions();
    }

    $('form fieldset > fieldset').each(function () {
        var id = $(this).find('.extra-charge-id');
        var parent = $(this).parent();
        var href = '/' + lang + '/company/' + companyId + '/cabinet/air/charge/extra-charge/remove/' + parent.attr(
                'id') + '/' + id.val();
        $(this).prepend(renderRemoveButton(href));
    });

    $('.extra-charge-remove').on('click', function (event) {
        event.preventDefault();
        if ($(this).attr('href') === '#') {
            $(this).parent().fadeOut(function () {
                $(this).remove();
            });
        } else {
            url = $(this).attr('href');
            $('#confirm').modal('show');
        }
    });

    $('.fieldset-remove').on('click', function (event) {
        event.preventDefault();
        $(this).parent().parent().fadeOut(function () {
            $(this).remove();
        });
    });

    $('#confirm-delete').on('click', function (event) {
        event.preventDefault();
        $.get(url, function (data) {
            if (data.success) {
                $('#' + data.type + ' fieldset').each(function () {
                    var id = $(this).find('.extra-charge-id');
                    if (id.val() == data.id) {
                        $(this).fadeOut(function () {
                            $(this).remove();
                        })
                    }
                });
            } else {
                alert(data.msg);
            }
        });
    });
});