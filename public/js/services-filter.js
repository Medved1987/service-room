var content;

$(function (){
    content = $('.services-holder tbody tr');
    $('.filtration-controls').on('click', '#byName', filterByName);
    $('.filtration-controls').on('click', '#byDate', filterByDate);
    $('.filtration-controls').on('click', '#byCountry', filterByCountry);
});

function filterByName(eve) {
    var $this = $(this);
    setActive($this);
    var filteredItems = content.sort(filter('.url'));
    replaceContent(filteredItems);
}

function filterByDate(eve) {
    var $this = $(this);
    setActive($this);
    var filteredItems = content.sort(function(prev, next){
        var re = /(\d\d).(\d\d)/;
        var prevText = $(prev).find('.connection-date').text().replace(re, '$2, $1');
        var nextText = $(next).find('.connection-date').text().replace(re, '$2, $1');
        return Date.parse(prevText) - Date.parse(nextText);
    });
    replaceContent(filteredItems);
}

function filterByCountry(eve) {
    var $this = $(this);
    setActive($this);
    var filteredItems = content.sort(filter('.country'));
    replaceContent(filteredItems);
}

function filter(className){
    return function(prev, next){
        var prevText = $(prev).find(className).text().toLowerCase();
        var nextText = $(next).find(className).text().toLowerCase();
        if(prevText === nextText){
            return 0;
        }

        if (prevText < nextText){
            return -1;
        }

        return 1;
    };
}

function setActive(btn){
    $('.filtration-controls button').removeClass('active');
    btn.addClass('active');
}

function replaceContent (filteredContent) {
    $('.services-holder tbody').html('');
    $('.services-holder tbody').append(filteredContent);
}

function logFiltrationResult(filteredContent){
    filteredContent.each(function () {
        console.log($(this).find('.url').text());
    });
}