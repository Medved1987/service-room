$(document).on('click', '.panel div.clickable', function (e) {
    var $this = $(this); //Heading
    var $panel = $this.parent('.panel');
    var $panel_body = $panel.children('.panel-body');
    var $display = $panel_body.css('display');

    if ($display == 'block') {
        $panel_body.slideUp();
    } else if($display == 'none') {
        $panel_body.slideDown();
    }
    // addon
    $this.find('span[class^="glyphicon glyphicon-chevron"]').toggleClass('glyphicon-chevron-up glyphicon-chevron-down');
    $this.parents('.user-profile-company-userlist').find('.glyphicon').toggleClass('glyphicon-minus glyphicon-plus');
    // END:addon
});
// подтверждение нажатия кнопки
$(document).ready(function(e){
    var $classy = '.panel.autocollapse';

    var $found = $($classy);
    $found.find('.panel-body').hide();
    $found.removeClass($classy);
});

