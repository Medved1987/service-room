<?php
return [
    'module_layouts'  => [
        //layout/layout for other
        
//        'Acex'                  => 'layout/uber',
        'Voodoo773Localization' => 'layout/uber',
//        'Exchange'              => 'layout/uber',
        'ZfcUserMod'            => 'layout/layout',
        'ZfcUser'               => 'layout/layout',
//        'Company'               => 'layout/user-profile',
//        'UserProfile'           => 'layout/user-profile',
//        'UserProfileMod'        => 'layout/user-profile',
//        'Admin'                 => 'layout/user-profile',
//        'CompanyUserAccess'     => 'layout/user-profile',
//        'CompanyAir'            => 'layout/user-profile',
    ],
    'service_manager' => [
        'invokables' => [
            'Zend\Session\SessionManager' => 'Zend\Session\SessionManager',
        ],
    ],
    
    'cli_config' => [
        'scheme' => 'http',
        'domain' => 'acex.time-ismoney.com',
    ],
];
