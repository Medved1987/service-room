<?php
return array(
    'upload' => array(
        'base_path' => './public',
        'upload'    => '/img/upload',
        'user'      => '/user',
        'company'   => '/company',
        'document'  => '/doc/upload',
        'carrier'   => '/carrier',
        'service'   => '/service',
        'exchange'  => '/exchange',
        'tmp'       => '/tmp',
    ),
);