<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ServiceRoom;



use ServiceRoom\Service\ImageManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;
//http://stackoverflow.com/questions/23117061/how-to-implement-beberlei-doctrine-extensions-in-zend-framework-2
return array(
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),

            )
        ),
        'configuration' => array(
            'orm_default' => array(
                'string_functions' => array(
                    'GroupConcat' => 'DoctrineExtensions\Query\Mysql\GroupConcat',
                    'year' => 'DoctrineExtensions\Query\Mysql\Year',
                    'day' => 'DoctrineExtensions\Query\Mysql\Day',
                    'month' => 'DoctrineExtensions\Query\Mysql\Month'

                )
            )
        )

    ),

    'controllers' => array(
        'invokables' => array(
            'ServiceRoom\Controller\Index' => Controller\IndexController::class,
            'ServiceRoom\Controller\Room' => Controller\RoomController::class,
        ),
        'factories' => array(
            //'ScnSocialKeys\Controller\User' => 'ScnSocialKeys\Service\UserControllerFactory',,

        ),
    ),
    'router' => array(
        'routes' => array(

            'service' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/:lang/service',
                    'defaults' => array(
                        '__NAMESPACE__' => 'ServiceRoom\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'lang'          => 'ru'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'item' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/[:id]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Index',
                                'action'        => 'index',
                                'id'            => 0
                            ),
                        ),
                    ),
                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/edit/[:id]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Index',
                                'action'        => 'edit',
                                'id'            => 0
                            ),
                        ),
                    ),
                    'delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/delete/[:id]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Index',
                                'action'        => 'delete',
                                'id'            => 0
                            ),
                        ),
                    ),
                    'new' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/new',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Index',
                                'action'        => 'new',
                            ),
                        ),
                    )
                )
            ),

            'service-room' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/:lang/room',
                    'defaults' => array(
                        '__NAMESPACE__' => 'ServiceRoom\Controller',
                        'controller'    => 'Room',
                        'action'        => 'index',
                        'lang'          => 'ru'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'item' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/[:hash]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Room',
                                'action'        => 'index',
                                'hash'            => 0
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'connect' => array(
                                'type'    => 'Literal',
                                'options' => array(
                                    'route' => '/connect',
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'ServiceRoom\Controller',
                                        'controller'    => 'Room',
                                        'action'        => 'connect',

                                    ),
                                ),
                            )
                        )
                    ),
                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/edit/[:hash]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Room',
                                'action'        => 'edit',
                                'hash'            => 0
                            ),
                        ),
                    ),
                    'delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/delete/[:hash]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Room',
                                'action'        => 'delete',
                                'hash'            => 0
                            ),
                        ),
                    ),
                    'new' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/new',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Room',
                                'action'        => 'new',
                            ),
                        ),
                    ),
                    'image' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/image/[:hash]/[:emailId]/[:dateImage]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Room',
                                'action'        => 'image',
                                'hash'          => 0,
                                'emailId'         => 0,
                                'dateImage'          => 0,//format 2017-01-03
                            ),
                        ),
                    ),
                )
            ),

            'json' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/:lang/json',
                    'defaults' => array(
                        '__NAMESPACE__' => 'ServiceRoom\Controller',
                        'controller'    => 'Index',
                        'action'        => 'indexjson',
                        'lang'          => 'ru'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'item_json' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/service/[:id]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Index',
                                'action'        => 'indexjson',
                                'id'            => 0
                            ),
                        ),
                    ),

                    'get_email' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/get_email',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Room',
                                'action'        => 'emailjson',
                                'id'            => 0
                            ),
                        ),
                    ),
                    'set_file' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/set_file',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Room',
                                'action'        => 'setFileJson'
                            ),
                        ),
                    ),
                    'test' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/test',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Room',
                                'action'        => 'testJson'
                            ),
                        ),
                    ),
                    'test2' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/test2',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceRoom\Controller',
                                'controller'    => 'Room',
                                'action'        => 'testJson2'
                            ),
                        ),
                    ),


                )
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
        ),
        'factories' => [
            //Service\ImageManager::class => InvokableFactory::class,
            //'Service\ImageManager' => ImageManager::class
        ],
        'invokables' =>[
            //'Service\ImageManager' => ImageManager::class
        ]
    ),
    'view_helpers' => array(
        'invokables' => array(
            //'socialSignInButton' => 'ScnSocialKeys\View\Helper\SocialSignInButton',
        ),

    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'service-room/index/index' => __DIR__ . '/../view/service/index/service.phtml',
            'service-room/index/edit' => __DIR__ . '/../view/service/index/service_edit.phtml',
            'service-room/index/new' => __DIR__ . '/../view/service/index/service_new.phtml',

            'service-room/room/index' => __DIR__ . '/../view/service/index/room.phtml',
            'service-room/room/new' => __DIR__ . '/../view/service/index/room_new.phtml',
            'service-room/room/edit' => __DIR__ . '/../view/service/index/room_edit.phtml',
            'service-room/room/connect' => __DIR__ . '/../view/service/index/room_connect.phtml',
            'service-room/room/image' =>  __DIR__ . '/../view/service/index/room_image.phtml',

            'service-room/room/test-json2' => __DIR__ . '/../view/service/index/form.phtml',



            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies'=>array('ViewJsonStrategy'),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
