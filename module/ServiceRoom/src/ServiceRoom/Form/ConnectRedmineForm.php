<?php
namespace ServiceRoom\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class ConnectRedmineForm extends Form
{
    public function __construct()
    {
        // Определяем имя формы
        parent::__construct('connect-redmine-form');

        // Задаем метод POST для этой формы
        $this->setAttribute('method', 'post');
        // Добавляем элементы формы
        $this->addinputFilter();
        $this->addElements();
    }

    private function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'url',
            'attributes' => [
                'class' => 'form-control'
            ],
            'options' => [
                'label' => 'Server redmine ',
            ],
        ]);
        $this->add([
            'type'  => 'text',
            'name' => 'login',
            'attributes' => [
                'class' => 'form-control'
            ],
            'options' => [
                'label' => 'Login ',
            ],
        ]);

        $this->add([
            'type'  => 'password',
            'name' => 'password',
            'attributes' => [
                'class' => 'form-control'
            ],
            'options' => [
                'label' => 'Password ',
            ],
        ]);
        $this->add([
            'type'  => 'hidden',
            'name' => 'connect',
        ]);


        $this->add([
            'type'  => 'submit',
            'name' => 'connect-redmine',
            'attributes' => [
                'value' => 'Подключиться ',
                'class' => 'btn btn-success btn-sm pull-right'
            ],
        ]);
    }
    private function addinputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        $inputFilter->add([
            'name' => 'login',
            'required' => true,


            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [   'name' => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 255
                    ],
                ],
            ],

        ]);
        $inputFilter->add([
            'name' => 'url',
            'required' => true,

            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [   'name' => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 255
                    ],
                ],
            ],

        ]);

        $inputFilter->add([
            'name' => 'password',
            'required' => true,


            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [   'name' => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 255
                    ],
                ],
            ],

        ]);


    }
}