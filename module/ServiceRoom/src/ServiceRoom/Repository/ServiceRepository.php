<?php

namespace ServiceRoom\Repository;

use ServiceRoom\Entity\Service;
use Doctrine\ORM\EntityRepository;
use Entities;


class ServiceRepository extends EntityRepository
{
    public function findService()
    {
        $result = [];
        $em = $this->getEntityManager();
        $queryBuilder = $em->createQueryBuilder();

        $queryBuilder->select('s')
            ->from(Service::class, 's');

        $service = $queryBuilder->getQuery()->getArrayResult();
        if ($service) {
            foreach ($service as $value) {
                $result[$value['id']] = $value['name'];
            }
        }

        return $result;

    }


}