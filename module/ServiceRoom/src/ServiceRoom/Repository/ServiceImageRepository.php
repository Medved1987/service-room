<?php

namespace ServiceRoom\Repository;

use ServiceRoom\Entity\Service;
use Doctrine\ORM\EntityRepository;
use Entities;
use ServiceRoom\Entity\ServiceImage;
use Zend\Debug\Debug;


class ServiceImageRepository extends EntityRepository
{
    public function findDate($date)
    {
        $result = [];
        $em = $this->getEntityManager();
        $createDate = explode("-",$date);
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('si')
            ->from(ServiceImage::class, 'si')
            //->where("CAST(si.createDate AS DATE) = CAST(:date AS DATE)")
            ->where('year(si.createDate) = :year')
            ->andWhere('month(si.createDate) = :month')
            ->andWhere('day(si.createDate) = :day')
            ->setParameter('year',$createDate[0])
            ->setParameter('month',$createDate[1])
            ->setParameter('day',$createDate[2])
        ;


        $image = $queryBuilder->getQuery()->getArrayResult();

        foreach ($image as $value){
            $date = $value['createDate']->format('Y-m-d H:i:s');

            $result[$date][]=$value;

        }
//        Debug::dump($result);
//        die;
        return $result;

    }


}