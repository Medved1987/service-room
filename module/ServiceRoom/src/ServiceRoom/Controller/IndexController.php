<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ServiceRoom\Controller;


use ServiceRoom\Entity\Service;
use ServiceRoom\Form\EditServiceForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{


    public function indexAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data['title'] = 'Service';
        $serviceId = $this->params()->fromRoute('id', 0);

        $data['service'] = [];

        if ($serviceId) {

            $serviceSingle = $em->getRepository(Service::class)->findOneBy(['id' => $serviceId]);

            $data['service']['show'] = $serviceSingle;
        }

        $service = $em->getRepository(Service::class)->findAll();
        if (!empty($service)) {
            foreach ($service as $value) {
                $data['service']['list'][] = [
                    'id' => $value->getId(),
                    'name' => $value->getName(),
                    'active' => ($serviceId == $value->getId()) ? true : false
                ];
            }
        }


        //return new JsonModel(['data'=>$data]);
        return new ViewModel(['data' => $data]);
    }

    public function editAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data['title'] = 'Service Edit';
        $serviceId = $this->params()->fromRoute('id', 0);

        $data['service'] = [];

        if ($serviceId) {
            $formEditService = new EditServiceForm();
            $serviceSingle = $em->getRepository(Service::class)->findOneBy(['id' => $serviceId]);
            $name = $formEditService->get('name');
            $name->setValue($serviceSingle->getName());
            if ($this->getRequest()->isPost()) {
                $dataForm = $this->params()->fromPost();
                $formEditService->setData($dataForm);
                if ($formEditService->isValid()) {
                    $dataForm = $formEditService->getData();
                    $serviceSingle->setName($dataForm['name']);
                    $em->flush();
                    $this->flashMessenger()->addSuccessMessage("Cохранен успешно");
                    return $this->redirect()->toRoute('service/edit', ['id' => $serviceId]);
                }
            }
            $data['service']['form'] = $formEditService;
        }

        $service = $em->getRepository(Service::class)->findAll();
        if (!empty($service)) {
            foreach ($service as $value) {
                $data['service']['list'][] = [
                    'id' => $value->getId(),
                    'name' => $value->getName(),
                    'active' => ($serviceId == $value->getId()) ? true : false
                ];
            }
        }


        //return new JsonModel(['data'=>$data]);
        return new ViewModel(['data' => $data]);
    }

    public function newAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data['title'] = 'Service New';


        $data['service'] = [];


            $formEditService = new EditServiceForm();
            $serviceSingle = new Service();

            if ($this->getRequest()->isPost()) {
                $dataForm = $this->params()->fromPost();
                $formEditService->setData($dataForm);
                if ($formEditService->isValid()) {
                    $dataForm = $formEditService->getData();
                    $serviceSingle->setName($dataForm['name']);
                    $em->persist($serviceSingle);
                    $em->flush();
                    $this->flashMessenger()->addSuccessMessage("Создан успешно");
                    return $this->redirect()->toRoute('service/item', ['id' => $serviceSingle->getId()]);
                }
            }
            $data['service']['form'] = $formEditService;


        $service = $em->getRepository(Service::class)->findAll();
        if (!empty($service)) {
            foreach ($service as $value) {
                $data['service']['list'][] = [
                    'id' => $value->getId(),
                    'name' => $value->getName(),
                    'active' => false
                ];
            }
        }


        //return new JsonModel(['data'=>$data]);
        return new ViewModel(['data' => $data]);
    }

    public function deleteAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $serviceId = $this->params()->fromRoute('id', 0);
        if ($serviceId) {
            $serviceSingle = $em->getRepository(Service::class)->findOneBy(['id' => $serviceId]);
            $em->remove($serviceSingle);
            $em->flush();
            $this->flashMessenger()->addSuccessMessage("Удален успешно");
        }

        return $this->redirect()->toRoute('service');
    }


    public function indexjsonAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();

        $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data['title'] = 'Service';
        $serviceId = $this->params()->fromRoute('id', 0);
        $service = $this->em->getRepository(Service::class)->findAll();

        $data['service'] = [];

        if ($serviceId) {
            $formEditService = [
                'input' => [
                    0 => [
                        'type' => 'text',
                        'name' => 'name',
                        'label' => 'Service name '
                    ],
                    1 => [
                        'type' => 'submit',
                        'name' => 'edit',
                        'value' => 'Сохранить ',
                        'class' => 'btn btn-success btn-sm pull-right'
                    ]
                ]
            ];

            $data['service']['form'] = $formEditService;
        }

        if (!empty($service)) {
            foreach ($service as $value) {
                $data['service']['list'][] = [
                    'id' => $value->getId(),
                    'name' => $value->getName(),
                    'active' => ($serviceId == $value->getId()) ? true : false
                ];
            }
        }

        return new JsonModel(['data' => $data]);
    }

    private function getUserId($route = null)
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            //redirct $route
        }
        return $this->zfcUserAuthentication()->getIdentity()->getId();
    }


}
