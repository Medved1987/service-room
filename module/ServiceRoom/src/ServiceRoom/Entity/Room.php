<?php
namespace ServiceRoom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="service_room")
 */
class Room
{


    /**
     * @var string
     * @ORM\Column(type="string", name="hash",length=255, nullable=true,unique=true)
     */
    private $hash;



    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="service_room_name",type="string", length=255, nullable=true)
     */
    private $serviceName;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="\ServiceRoom\Entity\Service", inversedBy="service")
     * @ORM\JoinColumn(name="service", referencedColumnName="id")
     */
    private $roomService;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="\ServiceRoom\Entity\User", inversedBy="userRoom")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    private $roomUser;



    /**
     * @return mixed
     */
    public function getRoomUser()
    {
        return $this->roomUser;
    }

    /**
     * @param mixed $roomUser
     */
    public function setRoomUser($roomUser)
    {
        $this->roomUser = $roomUser;
        $roomUser->addUserRoom($this);
    }



    /**
     * @return mixed
     */
    public function getRoomService()
    {
        return $this->roomService;
    }

    /**
     * @param mixed $roomService
     */
    public function setRoomService($roomService)
    {
        $this->roomService = $roomService;
        $roomService->addService($this);
    }






    /**
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * @param string $serviceName
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;
    }



    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }




}
