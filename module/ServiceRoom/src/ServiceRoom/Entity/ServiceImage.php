<?php
namespace ServiceRoom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ServiceRoom\Repository\ServiceImageRepository")
 * @ORM\Table(name="service_image")
 */
class ServiceImage{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="image",type="string", length=255,nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="\ServiceRoom\Entity\ServiceEmail", inversedBy="serviceEmail")
     * @ORM\JoinColumn(name="service_email_id", referencedColumnName="id")
     */
    private $serviceEmailId;


    /**
     * @ORM\Column(name="date" , nullable=true, type="datetime")
     */
    private $createDate;

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getServiceEmailId()
    {
        return $this->serviceEmailId;
    }

    /**
     * @param mixed $serviceEmailId
     */
    public function setServiceEmailId($serviceEmailId)
    {
        $this->serviceEmailId = $serviceEmailId;
        $serviceEmailId->addServiceImageId($this);
    }





}
