<?php
namespace ServiceRoom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ServiceRoom\Repository\ServiceRepository")
 * @ORM\Table(name="service_email")
 */
class ServiceEmail
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="email",type="string", length=50,nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="hash",type="string", length=255)
     */
    private $hash;

    /**
     * @var string
     * @ORM\Column(name="token",type="string", length=255)
     */
    private $token;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }


    /**
     * @ORM\ManyToOne(targetEntity="\ServiceRoom\Entity\User", inversedBy="userServiceEmail")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    private $serviceEmailUser;

    /**
     * @ORM\Column(name="date_c" , nullable=true,type="datetime")
     */
    private $createDate;
    /**
     * @return mixed
     */
    public function getServiceEmailUser()
    {
        return $this->serviceEmailUser;
    }
    /**
     * @param mixed $serviceEmailUser
     */
    public function setConnectUser($serviceEmailUser)
    {
        $this->serviceEmailUser = $serviceEmailUser;
        $serviceEmailUser->addUserServiceEmail($this);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @ORM\OneToMany(targetEntity="\ServiceRoom\Entity\ServiceImage", mappedBy="serviceEmailId")
     * @ORM\JoinColumn(name="id", referencedColumnName="service_email_id")
     */
    private $serviceImageId;

    public function __construct()
    {
        $this->serviceImageId = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getServiceImageId()
    {
        return $this->serviceImageId;
    }

    /**
     * @param mixed $service
     */
    public function addServiceImageId($serviceImageId)
    {
        $this->serviceImageId[] = $serviceImageId;
    }


}
