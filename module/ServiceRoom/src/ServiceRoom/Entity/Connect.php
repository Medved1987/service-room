<?php
namespace ServiceRoom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="service_connect")
 */
class Connect
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="connect",type="string", length=255)
     */
    private $connect;

    /**
     * @var string
     * @ORM\Column(name="api_key",type="string", length=255)
     */
    private $key;
    /**
     * @var string
     * @ORM\Column(name="room_hash",type="string", length=255)
     */
    private $hash;

    /**
     * @var string
     * @ORM\Column(name="url",type="string", length=255)
     */
    private $url;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\ServiceRoom\Entity\User", inversedBy="userConnect")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    private $connectUser;

    /**
     * @return mixed
     */
    public function getConnectUser()
    {
        return $this->connectUser;
    }

    /**
     * @param mixed $roomUser
     */
    public function setConnectUser($connectUser)
    {
        $this->connectUser = $connectUser;
        $connectUser->addUserConnect($this);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getConnect()
    {
        return $this->connect;
    }

    /**
     * @param string $connect
     */
    public function setConnect($connect)
    {
        $this->connect = $connect;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }





}
