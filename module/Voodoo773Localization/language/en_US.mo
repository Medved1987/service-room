��    |      �  �   �      x
  8   y
  "   �
  H   �
  2     7   Q  M   �     �  ,   �       6   9    p  {  r  �  �    �     �  <     �  J  [  �  5   0     f     s     �  
   �  �   �  y  @  c  �#     &     +&  *   I&     t&  %   �&  #   �&  
   �&     �&      �&  &  '  :  @)  b   {*     �*     �*  �  +  �   �,    �-  0   �.  $   �.  a   �.  �   P/     0     %0  �  ,0  w  5     �9     �9     �9  %   �9     �9     :     
:    $:  �  C<    �=  .   �?  K  +@  �  wC  R   E     bE     tE     �E     �E  !   �E     �E     �E     F  9   F     SF     `F  (   wF  �  �F     +H  '   8H     `H  "   nH  b   �H  �  �H  �  �J  $   6M  �   [M     �M  2   �M     'N     AN  {   XN  6   �N  {   O     �O     �O     �O  4   �O  .   P  (   6P     _P  �   {P  !   !Q  H  CQ  ,   �R     �R     �R  0   �R  +   S  *   /S  �  ZS     EU  =   [U     �U  -   �U     �U  *   gV  E   �V     �V  ,  �V  -   Y     <Y  A   KY  �  �Y     1[     Q[  .   `[     �[  %   �[  1   �[     �[     \     &\     2\  �  J\  �   ^    �^  	  �_  �   �`     Va    la  �  |c     e     e     e     !e     *e  H   6e  \  e  K  �g  
   (i     3i     @i  	   \i     fi     ui     �i     �i     �i    �i  �   �j  6   Wk     �k     �k  �   �k  �   Ol  r   �l     lm     |m  ,   �m  V   �m  
   n  
   n  �  #n  m  �p     s     "s     )s     1s     Bs     Os  	   Rs  
  \s  �   gt  �   du     bv  ]  uv  �   �w  +   �x  	   �x  	   �x     �x     �x      y     y      y     %y     +y     Cy     Ly     Yy  �   jy     =z     Cz  	   Yz     cz  &   sz  �   �z  *  }{     �|  8   �|     �|     �|     }     }  3   $}  "   X}  4   {}  
   �}     �}     �}     �}     �}     ~     !~  J   0~     {~  �   �~     0     D     R     Z     r     �  �   �     ��     ��     ��     Ѐ  3   �     �  %   0�     V�  ;  \�     ��     ��     ��         X       K   P   2      .   L   |   U   $      z   !   ,   A       v   %                    7   @   l   3   a                                n   '          +       4   j   <   8       x   i   -       "       t   k   g       >                F   :       _   d            (       r                          o          O   	   q   Q   h       D   #       f   E   Z   &   [   0          e                  V       *   W           G   M   B   6   T       ?   \   S      p   C           I   H       =       Y          w      9      N   u           /      m   
      {   1   s   ;   c   ^   R   5   b   J   y      )   ]             `        10. Модификация данной Политики 11. Связаться с нами 2. Неличная информация о нашем Веб-сайте 3. Использование информации 4. Раскрытие личной информации 5. Обзор и Доступ к Вашей личной Информации 6. Выбор/Отказ 7. Ссылки на другие сайты 8. Безопасность 9. Детская конфиденциальность ACEX24 - быстро развивающаяся

            логистическая информационная компания с

            более чем двадцатилетним опытом в сфере

            логистики. Предлагаемый сервис, который мы

            развиваем, в корне перевернет мир логистики,

            чтобы лучше обслуживать миллионы

            пользователей, которые остаются

            недовольными современными реалиями

            индустрии. Со штаб-квартирой в Москве,

            Россия, мы - частная компания, работающая,

            чтобы помочь бизнес-компаниям и частным

            лицам стать более продуктивными и

            квалифицированными. ACEX24 может изменить эту политику
конфиденциальности в любое время без
предварительного уведомления, разместив
обновленную политику конфиденциальности на
этом Сайте с указанием даты последнего
обновления. ACEX24 может получить определенную
комплексную информацию, даже известную как
демографические сведения и данные профиля
с нашего Веб-сайта. Эта информация помогает
нам в сборе сведений, таких как количество
Посетителей нашего Веб-сайта и количества
просмотренных ими страниц. ACEX24 не предоставляет информацию
несовершеннолетним и не принимает
информацию от несовершеннолетних детей.
Если Вы обнаружили, что Вы ошибочно
получили информацию от
несовершеннолетнего ребенка, Мы будем
немедленно прилагать все усилия, чтобы
удалить информацию из наших записей ACEX24 никогда не продаст, не сдаст, не
поделится, не обменяет Вашу личную
информацию без Вашего согласия, за
исключением случаев, изложенных в данном
документе. ACEX24 политика конфиденциальности ACEX24 серьезен в частном характере
персональной информации, такой как имя,
адрес, телефон, электронная почта,
полученной или предоставленной
посетителями ("Гость", "Вы", "Ваш" или
"Пользователь") нашему веб-сайту
www.acex24.net ("Веб-сайт"). Эта политика
конфиденциальности описывает пути
предоставления Вами Нам Вашей
персональной информации, виды личной
информации, которые Мы собираем, и как
ACEX трактует информацию, которую Мы
собираем, когда Вы посещаете Веб-сайт и/или
регистрируетесь как Пользователь. ACEX24, его филиалы и другие поставщики
услуг могут предоставить Вашу персональную
информацию в ответ на действительный
юридически законный запрос, применимый к
законодательству Российской Федерации. Мы
также можем раскрыть личную информацию,
когда это необходимо для создания,
осуществления или защиты правовых
притязаний, чтобы исследовать или
предотвратить фактическую или
предполагаемую потерю или вред людям или
собственности, или другому, допускаемое
законом. Cообщение успешно отправлено Metal Plates Авиа компании Аэропорты Бронь В данный момент по данному направлению нет данных, попробуйте пожалуйста позже... В дополнении к сбору личной и неличной
информации, ACEX24 может собирать
неличную, сводную информацию об
использовании данного Веб-сайта. Эта
информация не является личной и будет
использоваться только для поиска того, как
зарегистрированные Пользователи используют
наш сервис и Веб-сайт. Например, эта
информация будет говорить Нам сколько
времени проводит Пользователь на сайте, что
находят для себя Пользователи на сайте, и на
каких сайтах находятся наши пользователи. Сбор
подобной информации позволит Нам, среди
всего прочего, подготовиться к требованиям
нагрузки трафика и эффективной доставки
наших продуктов и услуг. В рамках сервиса, ACEX24 может создать
связи, позволяющие Вам получать доступ к
сторонним веб-сайтам. ACEX24 не несет
ответственности за содержимое, которое
появится на этих сайтах и не потверждает эти
сайты. Пожалуйста, обратитесь к политике
конфиденциальности этих сайтов, чтобы
определить, как относиться к пользовательской
информации. Ваш email Ваш комментарий Ваша заявка отправлена Ваше имя Ведомость индустрии Внутренние сервисы Войти Войти как клиент Войти как партнёр Вся информация, описанная выше хранится на
ограниченных серверах баз данных. Тем не
менее, Вы не должны предоставлять
конфиденциальную информацию на этом Веб-
сайте. Мы поддерживаем разумные
технические, физические и административные
меры предосторожности, чтобы помочь
защитить личную информацию. Вы должны написать Нам электронное письмо
через "Связаться с Нами" на нашем Веб-сайте.
Мы запросим Вашу контактную информацию,
чтобы ответить на Ваш запрос или
комментарий. Данный тип груза не поддерживает смешанную перевозку Добавить место Другое Если Вы захотите получать Анонсы Веб-сайта,
Пресс-релизы, Презентации, Службу
уведомлений или другие уведомления по
электронной почте, Мы запросим Выше имя и
электронную почту, чтобы предоставить Вам
эти уведомления. Если Мы присылаем Вам электронные письма
с информацией о новых продуктах или услугах,
Вы сможете отказаться от будущих
уведомлений. Если у Вас есть какие-либо вопросы или
комментарии об этой Политике
Конфиденциальности, или нашем Веб-сайте в
общем, пожалуйста, свяжитесь с нами по: Запрос торговой стоимости Зарегистрироваться Заявление о конфиденциальности компании-партнера ACEX Здесь ACEX24 было бы интересно узнать, что выхотите сказать. Напишите нам пару строк или скажите привет Идет отправка... Имя Как большинство Веб-сайтов, ACEX24
пользуется сервером “cookies”. Cookie - это
маленький текстовой документ, который часто
включает анонимный уникальный
идентификатор. Когда Вы посещаете веб-сайт,
компьютер сайта запрашивает Ваш компьютер
разрешение сохранить этот файл в части
Вашего жесткого диска, который специально
предназначен для cookies. Каждый веб-сайт
может послать собственный cookie Вашему
браузеру, если предпочтения Вашего браузера
позволяют сделать это, но (чтобы защитить
Вашу приватность) Ваш браузер позволяет
веб-сайту открыть доступ к cookies, которые он
уже Вам посылал, а не те cookies,
отправленные Вам другими сайтами. Вы
можете управлять или отключить cookies
инструментами Вашего браузера Как только Вы загрузите Наш Веб-сайт, он
использует cookies, чтобы отличить Вас от
других пользователей, чтобы уберечь Вас от
просмотра ненужной рекламы или требуя,
чтобы Вы зарегистрировались там, где не
нужно. Cookies, в сочетании с нашими лог
файлами веб-сервера, позволяют Нам
вычислить суммарное количество людей,
посещающих наш Веб-сайт и проследить, какие
его части являются самыми популярными. Это
помогает Нам собрать обратную связь, чтобы
постоянно улучшать наш Веб-сайт и лучше
обслуживать наших клиентов. ACEX никогда не
использует cookies для получения информации
с компьютера, которая не связана с нашим
Сайтом или услугами. Карьера Клиент Комментарий Компания перевозчик Контакты Куда Морские Порты Мы можем передать (или иным способом
распространить) Вашу персональную
информацию нашим филиалам и другим
третьим лицам, которые предоставляют услуги
от нашего имени. Ваша персональная
информация может быть сохранена и
обработана нашими филиалами и другими
поставщиками услуг третьей стороны. Мы можем поделиться этой информацией с
другими, такими, как рекламодатели, которые
заинтересованы в размещении рекламы на
нашем Сайте, в анонимной форме, что
означает, что информация не будет содержать
каких-либо персональных данных о Вас. Мы также можем использовать эти сведения,
чтобы улучшить наш Сайт и/или применить
Ваш опыт на нашем Сайте, показав Ваше
содержимое, в котором Мы можем быть
заинтересованы, и отображая содержимое, в
котором Вы можете быть заинтересованы. Мы
получим эти данные через технологии "Cookie". Начните пользоваться ACEX24 Наша команда разнообразна, сильно

            мотивирована, трудолюбива и позитивно

            конкурентноспособна. Мы создали счастливый,

            быстро растущий, крепкий стартап рабочего

            пространства, который стремится к

            удовлетворению наших высоких внутренних

            ожиданий. Кандидаты должны быть

            увлеченными перфекционистами, которые не

            остановятся ни перед чем, чтобы прекрасно

            выполнить свою работу. Иные нам не подходят. Нашим поставщикам услуг предоставляется
информация, которая необходима им для
выполнения поставленных перед ними задач, и
мы не разрешаем им раскрывать личную
информацию для их собственного продвижения
или других целей. Необходимо будет указать адрес забора груза! Номер ООН Номер ООН  Опасный груз Основной груз  Особенности груза Отделенный груз  Откуда Очистить Ошибка! Cообщение не отправлено Пароль Перевозчики Перейти в калькулятор Персональная информация относится к любой
информации в отношении одного из
идентифицируемой личности. Эта информация
может быть получена через этот Веб-
сайт,www.acex24.net, или одним из наших веб-
сайтов, соответственно: Печать Подтверждение пароля Поиск... Получение данных... Правовые вопросы, соблюдение и
предотвращение потерь Предоставляя ACEX24 любую информацию, Вы
соглашаетесь с использованием нами
информации, изложенной в настоящей
Политике Конфиденциальности. Если Вы не
согласны с условиями ниже, пожалуйста, не
предоставляйте Нам эту информацию Продолжая, я соглашаюсь с тем, что компания ACEX или ее представители
                    могут связаться со мной при помощи эл. почты, телефона 
                    или СМС (включая автоматические средства), воспользовавшись указанными 
                    мной эл. адресом или номером телефона, в том числе в маркетинговых целях. 
                    Я подтверждаю, что прочитал(а) и понял(а) соответствующее  Разместить на бирже Размещение на бирже доступно только зарегистрированным пользователям! Рассчитать Расчет стоимости перевозки Расчитали как Регистрация Резервирование доступно только зарегистрированным пользователям! Сбор персональной информации Свяжитесь с нами, если Вы заинтересовались
работой в нашей команде. Связаться с Нами Связаться с нами Сервис Следите за своим заработком. Соглашение Услуг Сервиса Создать новый аккаунт Сроки доставки Станьте партнером ACEX24 и зарабатывайте хорошие деньги в качестве независимого подрядчика. Сторонние сервисы Также могут быть и другие места на нашем
Веб-сайте, где Мы можем запросить Вашу
личную информацю. Мы будем запрашивать
личную информацию только когда это
действительно необходимо Текущая Рыночная Ставка Телефон: Тема Топ Логистичеких компаний Транспортные агенты NVOCC Транспортные агенты VOCC У Вас есть право на доступ, обновление или
исправление неточностей в Вашей личной
информации под нашим заключением и
контролем, с учетом некоторых исключений,
установленных законом. Вы можете запросить
доступ, связавшись с нами, используя
контактную информацию ниже. Убрать груз Уведомления по электронной почте Уже есть аккаунт? Укажите класс опасности  Управляйте способами оплаты, просматривайте историю и многое другое. Уточните у перевозчика Химки, 141400, Московская область, Россия Цена Чтобы разместить Вас с Вашим запросом
торговой стоимости, предоставить Вам
дополнительные услуги, а также отправлять
информацию об услугах ACEX24 в Вашем
местоположении, мы можем запросить Ваше
имя, географические координаты, номер
телефона и другую личную информацию,
которую Вы прямо не предоставляете. Экспедиторские компании записей нуждается в таких партнерах, как вы. Project-Id-Version: 
POT-Creation-Date: 2017-04-17 11:45+0300
PO-Revision-Date: 2017-04-17 11:47+0300
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: translate
X-Poedit-SearchPath-0: .
 10. Modification to this Policy 11. Contact Us 2. Non-Personal Information About Our Web site 3. Usage Information 4. Disclosure of Personal Information 5. Review and Access to Your Personal Information 6. Choice/Opt-Out 7. Links to Other Sites 8. Security 9.	Children’s Privacy ACEX24 is a rapidly growing logistics information company with over 30 years of logistical experience. The service offering we have developed is turning the logistics world on its head, in order to better serve the millions of users who are left “unsatisfied” with the current realities of the industry. Headquartered in Moscow, Russia we are a privately held company working to help businesses and individuals become more effective and efficient. ACEX24 may change this Privacy Policy at any time without prior notice to You by posting an updated Privacy Policy on this Site indicating the date of the most recent update. ACEX24 may obtain certain aggregate and non-personally identifiable information, also known as demographic and profile data, from our Web site. This information assists Us in collecting data such as how many Visitors We had to our Web site and the pages viewed. ACEX24 does not market to minor children nor does it knowingly collect information from minor children. If We discover that We have mistakenly received information from a minor child, We will make every effort to immediately remove the information from our records. ACEX24 will never sell, rent, share, trade or barter Your personal information without Your consent, except as otherwise set out herein. ACEX24 Privacy Policy ACEX24 is serious about the private nature of personal information, such as the name, address, telephone number and email address, obtained from, or provided by, visitors (“Visitor”, “You”, “Your” or “User” ) to our website www.acex24.net (“Web site”). This Privacy Policy describes the ways in which You provide Us with Your personal information, the types of personal information We collect, and how Paradigm Freight treats the information We collect when You visit the Web site and/or register as a User. ACEX24, its affiliates and other service providers may provide Your personal information in response to a legally valid inquiry or order as applicable to RF law. We may also disclose personal information where necessary for the establishment, exercise or defense of legal claims, to investigate or prevent actual or suspect loss or harm to persons or property, or as otherwise permitted by law. Success Message   Airline Airports Reservation Currently there is no data in this direction, please try again later ... In addition to collecting personal and non-personal information, ACEX24 may collect non-personal, aggregated information about use of this Web Site. This information is not personally identifiable and only will be used to find out how registered Users use our service and Web site. For example, this information will tell Us how much time Users spend on the site, what web sites our Users are coming from, and to what web sites Our Users are going. The collection of this information allows Us to, among other things, prepare for traffic load demands and to efficiently deliver our products and services. As part of the service, ACEX24 may create links allowing You to access third-party web sites. Paradigm Freight is not responsible for the content that appears on those web sites and does not endorse those websites. Please consult those web sites’ individual privacy policies in order to determine how they treat user information. Your email Your comment Your message has been sent. Your name Industry Lists Internal services Sign in Login as a Client Login as a Partner All information described above is stored on restricted database servers. However, You should not provide any confidential information on this Web site. We maintain reasonable technical, physical and administrative safeguards to help protect personal information. Should You email Us through the “Contact Us” link on our Web site, We request Your contact information in order to respond to Your inquiry or comment. This type of cargo does not support combined transport + item Other If You wish to receive Web site Announcements, Press Releases, Presentations, Service Updates or other Email alerts, We require Your name and email to provide these alerts to You. If We ever send You email alerts or information by email concerning new products or services, We will provide You with an opportunity to unsubscribe from future notices. If You have any questions or comments about this Privacy Policy, or our Web site in general, please contact us by: Bidding Queries Register now Privacy statement of an ACEX partner company Here are ACEX24 we're interested in what you have to say. Drop us a line or say hello. Sending... First Name Like most web sites, ACEX24 makes use of browser “cookies.” A cookie is a very small text document, which often includes an anonymous unique identifier. When You visit a web site that site's computer asks Your computer for permission to store this file in a part of Your hard drive specifically designated for cookies. Each web site can send its own cookie to Your browser if Your browser's preferences allow it, but (to protect Your privacy) Your browser only permits a web site to access the cookies it has already sent to You, not the cookies sent to You by other sites. You can manage or disable Your cookies by using tools of Your browser. As You browse our Web site, the Web site uses its cookies to differentiate You from other Users in order to prevent You from seeing unnecessary advertisements or requiring You to log in more than is necessary for security. Cookies, in conjunction with our web server's log files, allow Us to calculate the aggregate number of people visiting our Web site and which parts of the Web site are most popular. This helps Us gather feedback in order to constantly improve our Web site and better serve our customers. ACEX will never use cookies to retrieve information from a computer that is unrelated to our Site or services. Careers Client Comment Shipping company Contact Info TO Sea Ports We may transfer (or otherwise make available) Your personal information to our affiliates and other third parties who provide services on Our behalf. Your personal information may be maintained and processed by our affiliates and other third party service providers. We may share this information with others, such as advertisers interested in advertising on our Site, in aggregate, anonymous form, which means that the information will not contain any personally identifiable information about You or any other person. We may use this data to improve our Site and/or tailor Your experience at our Site, showing You content in which We think You might be interested, and displaying content according to Your preferences. We obtain this data through “Cookie” technology. Start using ACEX24 Our team is diverse, highly motivated, hard-working, and positively competitive. We have created a happy, fast paced, and robust start-up work environment committed to meeting our high internal expectations. Candidates should fit the description of passionate perfectionists who will stop at nothing to get great wok done. All others need not apply. Our service providers are given the information they need to perform their designated functions, and We do not authorize them to use or disclose personal information for their own marketing or other purposes. You will need to specify residential pickup UN Number UN Number Dangerous goods Main cargo  Cargo features Separated cargo  From Clear Error! Message not sent Password Transporters Go to calculator Personal information relates to any information with regard to a single identifiable individual. This information may be obtained through this Web site, www.acex24.net, or one of our other websites, as follows: Print Password confirmation Search... Fetching Data.. Legal, Compliance and Loss Prevention: By providing to ACEX24 any such information, You agree for us to use of that information as set out in this Privacy Policy. If You do not agree to the conditions set forth below, please do not provide Us with this information. By continuing, I agree with the fact that the company ACEX or its representatives may contact me using e-mail. mail, phone or SMS (including automatic means), using the e-mail address or phone number specified by me, including marketing purposes. I confirm that I have read and realized appropriate Post in Exchange Post to exchange is only available for registered users! Get rate Freight calculator Calculate as Register Reservation is only available for registered users! Collection of Personal Information CONTACT US  if you are interested in working with us Contact Us Connect with us Service Keep track of your earnings. Service Provider Arrangements Create new account Delivery terms Become an ACEX24 partner and earn good money as an independent contractor. 3rd Party services There may be other areas throughout our Web site where We ask for Your personal information. We will only request this personal information when it is necessary. Current Market Rate By telephone: Subject Top logistics companies Transport agents NVOCC Transport agents VOCC You have the right to access, update, and correct inaccuracies in Your personal information in our custody and control, subject to certain exceptions prescribed by law. You may request access by contacting Us using the contact information set out below. Remove place Email Alerts Already have an account? Specify hazard class Manage payment options, view history and many more. Check with the carrier Khimki, Moscow region, 141400, Russia Price To supply You with Your bidding query, provide You with additional services, and send product information about ACEX24 services to Your location, we may request Your name, geographical coordinates, telephone number and email. ACEX24 does not collect any other personal information that You do not expressly provide. Forwarding company records needs partners like you. 