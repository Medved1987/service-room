<?php
namespace Voodoo773Localization\Entity;

class Upload
{
    protected $iata;
    protected $nameRus;
    protected $nameEng;
    protected $cityRus;
    protected $cityEng;
    protected $isoCode;
    protected $latitude;
    protected $longitude;
    
    /**
     * @return mixed
     */
    public function getIata()
    {
        return $this->iata;
    }
    
    /**
     * @param mixed $iata
     */
    public function setIata($iata)
    {
        $this->iata = $iata;
    }
    
    /**
     * @return mixed
     */
    public function getNameRus()
    {
        return $this->nameRus;
    }
    
    /**
     * @param mixed $nameRus
     */
    public function setNameRus($nameRus)
    {
        $this->nameRus = $nameRus;
    }
    
    /**
     * @return mixed
     */
    public function getNameEng()
    {
        return $this->nameEng;
    }
    
    /**
     * @param mixed $nameEng
     */
    public function setNameEng($nameEng)
    {
        $this->nameEng = $nameEng;
    }
    
    /**
     * @return mixed
     */
    public function getCityRus()
    {
        return $this->cityRus;
    }
    
    /**
     * @param mixed $cityRus
     */
    public function setCityRus($cityRus)
    {
        $this->cityRus = $cityRus;
    }
    
    /**
     * @return mixed
     */
    public function getCityEng()
    {
        return $this->cityEng;
    }
    
    /**
     * @param mixed $cityEng
     */
    public function setCityEng($cityEng)
    {
        $this->cityEng = $cityEng;
    }
    
    /**
     * @return mixed
     */
    public function getIsoCode()
    {
        return $this->isoCode;
    }
    
    /**
     * @param mixed $isoCode
     */
    public function setIsoCode($isoCode)
    {
        $this->isoCode = $isoCode;
    }
    
    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }
    
    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }
    
    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
    
    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }
}
