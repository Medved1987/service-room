<?php
namespace Voodoo773Localization\Form;

use Zend\InputFilter\InputFilter;

class CareerFilter extends InputFilter
{
    public function __construct(){
        $this->add(array(
            'name' => 'name',
            'filters' => array(
                array(
                    'name' => 'StripTags',
                ),
                array(
                    'name' => 'StringTrim',
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 20,
                    ),
                ),
                array(
                    'name' => 'Alpha',
                    'options' => array(
                        'allowWhiteSpace' => true,
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name' => 'email',
            'filters' => array(
                array(
                    'name' => 'StripTags',
                ),
                array(
                    'name' => 'StringTrim',
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ),
                ),
                array(
                    'name' => 'EmailAddress',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'comment',
            'filters' => array(
                array(
                    'name' => 'StripTags',
                ),
                array(
                    'name' => 'StringTrim',
                ),
            ),
        ));
    }
}

