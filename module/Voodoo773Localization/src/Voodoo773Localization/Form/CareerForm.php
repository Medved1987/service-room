<?php
namespace Voodoo773Localization\Form;

use Zend\Form\Form;

class CareerForm extends Form
{
    public function __construct(){
        parent::__construct('Career');
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'        => 'text',
                'placeholder' => 'Ваше имя',
                'required'    => true,
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'        => 'email',
                'placeholder' => 'Ваш email',
                'required'    => true,
            ),
        ));
        $this->add(array(
            'name' => 'comment',
            'type' => 'textarea',
            'attributes' => array(
                'placeholder' => 'Ваш комментарий',
                'rows'        => '4',
                'required'    => true,
            ),
        ));
        
        $this->add(array(
            'type' => 'button',
            'name' => 'submit',
            'options' => array(
                'label' => 'Отправить',
            ),
            'attributes' => array(
                'type'  => 'submit',
                'id'    => 'submit',
            )
        ));
    }
}

