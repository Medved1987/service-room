<?php

namespace Voodoo773Localization\View\Helper;

use Zend\View\Helper\AbstractHelper;

class RouteName extends AbstractHelper
{
    protected $routeMatch;
    
    public function __construct(\Zend\Mvc\Router\RouteMatch $routeMatch)
    {
        $this->routeMatch = $routeMatch;
    }
    
    public function __invoke()
    {
        if ($this->routeMatch) {
            return $this->routeMatch->getMatchedRouteName();
        }
    }
}