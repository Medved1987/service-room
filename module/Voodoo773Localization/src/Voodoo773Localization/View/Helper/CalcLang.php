<?php
namespace Voodoo773Localization\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container as ContainerSession;
class CalcLang extends AbstractHelper
{
    /**
     * Ru только если выбранно, иначе En
     * */
    function __invoke()
    {
        $SessionContainer = new ContainerSession();
        
        switch ($SessionContainer->lang) {
            case 'ru':
                return 'ru';
            default:
                return 'en';
        }
    }
}