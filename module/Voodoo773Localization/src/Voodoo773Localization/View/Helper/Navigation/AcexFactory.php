<?php
namespace Voodoo773Localization\View\Helper\Navigation;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AcexFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $navigation = new Acex();
        
        return $navigation->createService($serviceLocator);
    }
}
