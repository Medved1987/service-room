<?php
namespace Voodoo773Localization\View\Helper\Navigation;

use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class Lang extends DefaultNavigationFactory
{
    protected function getName()
    {
        return 'language';
    }
    
    protected function getPages(ServiceLocatorInterface $serviceLocator)
    {
        if (null === $this->pages) {
            $configuration = $serviceLocator->get('Config');
            
            if (!isset($configuration['navigation'])) {
                throw new \InvalidArgumentException('Could not find navigation configuration key');
            }
            if (!isset($configuration['navigation'][$this->getName()])) {
                throw new \InvalidArgumentException(sprintf(
                    'Failed to find a navigation container by the name "%s"',
                    $this->getName()
                ));
            }
            
            $pages = $this->getPagesFromConfig($configuration['navigation'][$this->getName()]);
            $this->pages = $this->preparePages($serviceLocator, $pages);
            
            $currentRoute = $serviceLocator->get('ViewHelperManager')->get('CurrentRoute');
            
            foreach ($this->pages as $num => $page) {
                $this->pages[$num]['route'] = $currentRoute();
            }
        }
        
        return $this->pages;
    }
}
