<?php
namespace Voodoo773Localization\View\Helper\Navigation;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Voodoo773Localization\View\Helper\Navigation\Resources;

class ResourcesFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $navigation = new Resources();
        
        return $navigation->createService($serviceLocator);
    }
    
}
