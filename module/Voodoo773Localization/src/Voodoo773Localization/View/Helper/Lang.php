<?php
namespace Voodoo773Localization\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container as ContainerSession;
class Lang extends AbstractHelper
{
    function __invoke()
    {
        $SessionContainer = new ContainerSession();
        if ($SessionContainer->lang == 'pt_BR') {
            return 'pt';
        }
        return $SessionContainer->lang;
    }
}