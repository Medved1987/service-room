<?php
namespace Voodoo773Localization\View\Helper;

use Zend\View\Helper\AbstractHelper;

class OrderExchangeIfAuth extends AbstractHelper
{

    public function __invoke($user_id){
        
        $reg_notification = [];
                
        if(!$user_id){
            $url        = $this->getView()->plugin('url');
            $translate  = $this->getView()->plugin('translate');
            $registrationLink = '<a class="calculator-modal-auth-btn" href="'.$url('zfcuser/register').'">'.$translate('Зарегистрироваться').'</a>';
            $loginLink = '<a class="calculator-modal-auth-btn" href="'.$url('zfcuser/login').'">'.$translate('Войти').'</a>';
            $bottomLinks = '<p>'.$loginLink.' '.$registrationLink.'</p>';
			$reg_notification['reservation'] = '<p>'.$translate('Резервирование доступно только зарегистрированным пользователям!').'</p>'.$bottomLinks;
			$reg_notification['exchange']    = '<p>'.$translate('Размещение на бирже доступно только зарегистрированным пользователям!').'</p>'.$bottomLinks;
        }
        else{
            ob_start();
            ?>
    			<div class="description">
    				<p><?php echo $this->getView()->translate('Do you want ACEX to talk to one of our shipping partners to give you an actual quote');?></p>
    			</div>
    			<form action="" id="reservationForm" class="reservation-form">
    				<div class="element-block">
    					<label class="title"><?php echo $this->getView()->translate('Description'); ?></label>
    					<textarea name="description" cols="30" rows="10" class="form-element"
    						placeholder="<?php echo $this->getView()->translate('Extra information you want to tell us');?>"></textarea>
    				</div>
    				<div class="btn-block">
    					<input type="submit" value="<?php echo $this->getView()->translate('Reserve'); ?>" class="submit-btn btn reservation-form-submit">
    					<input type="reset" value="<?php echo $this->getView()->translate('Cancel'); ?>" class="cancel-btn btn">
    				</div>
    			</form>
            <?php 
            $reg_notification['reservation'] = ob_get_clean();
            
            ob_start();
            ?>
				<div class="description">
					<p><?php echo $this->getView()->translate('Do you want ACEX to talk to one of our shipping partners to give you an actual quote'); ?></p>
				</div>
				<form action="" id="exchangeForm" class="reservation-form">
					<div class="element-block">
						<label class="title"><?php echo $this->getView()->translate('Title'); ?>*</label>
						<input type="text" name="title" class="form-element" placeholder='<?php echo $this->getView()->translate('Title'); ?>' required>
					</div>
					<div class="element-block">
						<label class="title"><?php echo $this->getView()->translate('Description');?></label>
						<textarea name="description" cols="30" rows="10" class="form-element" placeholder="<?php echo $this->getView()->translate('Extra information you want to tell us'); ?>"></textarea>
					</div>
					<div class="btn-block">
						<input type="submit" value="<?php echo $this->getView()->translate('Reserve'); ?>" class="submit-btn btn exchange-form-submit">
						<input type="reset" value="<?php echo $this->getView()->translate('Cancel'); ?>" class="cancel-btn btn">
					</div>
				</form>
            <?php 
            $reg_notification['exchange'] = ob_get_clean();
        }
        
	   return $reg_notification;
    }
}

