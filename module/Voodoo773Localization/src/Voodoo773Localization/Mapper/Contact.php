<?php 
namespace Voodoo773Localization\Mapper;

use Acex\Mapper\AbstractDbMapperWithEntity;

class Contact extends AbstractDbMapperWithEntity
{
    protected $tableName = 'contact';
}
