<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Voodoo773Localization\Controller\Index'
                => 'Voodoo773Localization\Controller\IndexController'
        ),
        'factories' => array(
            'my-controller' => function($sm) {
                $translator = $sm->getServiceLocator()->get('translator');
                $controller = new Voodoo773Localization\Controller\IndexController($translator);
            }
        ),
    ),
    'translator' => array(
        'event_manager_enabled' => true,
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'ScnSocialKeys\Controller',
                        'controller'    => 'Index',
                        'action'     => 'index',
                    ),
                ),
            ),

            'localization' => array(
                'type'    => 'Segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/:lang',
                    'constraints' => array(
                        'lang' => 'ru|en|es|it|de|pt|fr',
                    ),
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'ScnSocialKeys\Controller',//'Voodoo773Localization\Controller', 'ScnSocialKeys\Controller'
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'lang'          => 'ru',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => [
                    //
                    'resources' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/resources',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'resources',
                            ],
                        ],
                    ],
                    
                    'upload' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/upload',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'upload',
                            ],
                        ],
                    ],
                    
                    'careers' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/careers',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'careers',
                            ],
                        ],
                    ],
                    
                    'contact' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/contact',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'contact',
                            ],
                        ],
                    ],
                    
                    'lists' => [//Industry Lists
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/lists',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'lists',
                            ],
                        ],
                    ],
                    
                    'privacy' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/privacy',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'privacy',
                            ],
                        ],
                    ],
                    
                    'about' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/about',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'about',
                            ],
                        ],
                    ],
                   
                    'sign-in' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/sign-in',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'signIn',
                            ],
                        ],
                    ],
                    'become-partner' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/become-partner',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'becomePartner',
                            ],
                        ],
                    ],
                    'start-using' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/start-using',
                            'defaults' => [
                                'controller'    => 'Index',
                                'action'        => 'startUsing',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/partner' => __DIR__ . '/../view/layout/partner.phtml'
        ),
        'template_path_stack' => array(
            'Voodoo773Localization' => __DIR__ . '/../view',
        ),
    ),
);
