<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ScnSocialKeys;



use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

return array(
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            )
        ),

        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host'     => 'localhost.keysocial.com',
                    'port'     => '3306',
                    'user'     => 'root',
                    'password' => '',
                    'dbname'   => 'key_social',
                )
            )
        )
    ),

    'controllers' => array(
        'invokables' => array(
            'ScnSocialKeys\Controller\Index' => Controller\IndexController::class,
            'ScnSocialKeys\Controller\Rest' => Controller\RestController::class,
        ),
        'factories' => array(
            'ScnSocialKeys\Controller\User' => 'ScnSocialKeys\Service\UserControllerFactory',
            'ScnSocialKeys\Controller\Soc' => 'ScnSocialKeys\Service\UserSocControllerFactory',

        ),
    ),
    'router' => array(
        'routes' => array(

            'scn-social-auth-user' => array(
                'child_routes' => array(
                    'authenticate' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/authenticate',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ScnSocialKeys\Controller',
                                'controller'    => 'User',
                                'action'     => 'authenticate',
                            ),
                        ),
                        'child_routes' => array(
                            'provider' => array(
                                'type' => 'Segment',
                                'options' => array(
                                    'route' => '/:provider',
                                    'constraints' => array(
                                        'provider' => '[a-zA-Z][a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'ScnSocialKeys\Controller',
                                        'controller'    => 'Soc',
                                        'action'     => 'provider-authenticate',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),

            /*----------------------------*/

            'keys' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/:lang/user/key',
                    'defaults' => array(
                        '__NAMESPACE__' => 'ScnSocialKeys\Controller',
                        'controller'    => 'Soc',
                        'action'        => 'keys',
                        'lang'          => 'ru'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'keys_delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/delete/[:provider]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ScnSocialKeys\Controller',
                                'controller'    => 'Index',
                                'action'        => 'delete',
                                'provider'   =>""
                            ),
                        ),
                        )
                )
            ),
            'index_test' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/:lang/user/rest[/:id]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'ScnSocialKeys\Controller',
                        'controller'    => 'Rest',
                        //'action'        => 'index',
                        'lang'          => 'ru'
                    ),
                )
            ),



        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
        ),
        'factories' => array(
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'socialSignInButton' => 'ScnSocialKeys\View\Helper\SocialSignInButton',
            'providers' => 'ScnSocialKeys\View\Helper\Providers'
        ),

    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'scn-social-keys/soc/keys' => __DIR__ . '/../view/keys/index/index.phtml',
            'scn-social-keys/index/index' => __DIR__ . '/../view/keys/index/home.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies'=>array('ViewJsonStrategy'),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
