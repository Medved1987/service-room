<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ScnSocialKeys;

use ScnSocialKeys\Entity\Email;
use ScnSocialKeys\Entity\User;
use ScnSocialKeys\Entity\UserKey;
use ScnSocialKeys\Entity\UserEmail;
use ScnSocialKeys\Entity\UserProvider;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container as ContainerSession;

class Module
{


    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);


        $sm = $e->getApplication()->getServiceManager();

        $events = $e->getApplication()->getEventManager()->getSharedManager();

        $events->attach('ScnSocialAuth\Authentication\Adapter\HybridAuth', 'scnUpdateUser.post', function ($e) use ($sm) {
            $user = $e->getParam('user');

            $containerSession = new ContainerSession();

            $UserMapper = $sm->get('zfcuser_user_mapper');

            if ($containerSession->offsetGet('userCurrentEmail')) {
                $User = $UserMapper->findById($user->getId());
                $User->setEmail($containerSession->offsetGet('userCurrentEmail'));
                $UserMapper->update($User);
            }
            if (empty($containerSession->offsetGet('email'))) {
                $containerSession->offsetUnset('email');
            }

            $containerSession->offsetUnset('userCurrentEmail');
        });


        $events->attach('ScnSocialAuth\Authentication\Adapter\HybridAuth', 'scnUpdateUser.pre', function ($e) use ($sm) {

            $em = $sm->get('Doctrine\ORM\EntityManager');

            $containerSession = new ContainerSession();
            $provider = $e->getParam('provider');
            $user = $e->getParam('user');
            $containerSession->offsetSet('email', $user->getEmail());
            $isNewProvider = $em->getRepository(User::class)->isNewProvider($user->getId(), $provider);
            $user1 = $em->getRepository(User::class)->findOneBy(['email' => $user->getEmail()]);

            if (!empty($user1)) {
                $user->setId($user1->getId());
                $user->setPassword($user1->getPassword());
                $e->setParam('user', $user);
            }

            $userOld = $em->getRepository(User::class)->findOneBy(['id' => $user->getId()]);


            $containerSession->isNewProvider = $isNewProvider;

            if ($userOld) {
                $containerSession->offsetSet('userCurrentEmail', $userOld->getEmail());
            }

        });


        $events->attach('ZfcUser\Authentication\Adapter\AdapterChain', 'authenticate.pre', function ($e) use ($sm) {

            $session1 = new ContainerSession("Zend_Auth");

            if (isset($session1->storage)) {
                $session1->userId = $session1->storage;

            }

        });

        $events->attach('ZfcUser\Authentication\Adapter\AdapterChain', 'authenticate', function ($e) use ($sm) {
            $session1 = new ContainerSession("Zend_Auth");
        });
            $events->attach('ZfcUser\Authentication\Adapter\AdapterChain', 'authenticate.success', function ($e) use ($sm) {
            $session1 = new ContainerSession("Zend_Auth");
            $containerSession = new ContainerSession();
            $em = $sm->get('Doctrine\ORM\EntityManager');
            $userId = $e->getIdentity();
            $user = $em->getRepository(User::class)->findOneBy(['id' => $userId]);
            $request = $e->getRequest();
            $provider = $request->getMetadata('provider');
            $emailUser = $user->getEmail();
            $email = null;
            $isNewProvider = $em->getRepository(User::class)->isNewProvider($userId, $provider);
            if (!isset($containerSession->isNewProvider)) {
                $containerSession->isNewProvider = $isNewProvider;
            }
//            $email = ($provider && !$email)?
//                ($containerSession->offsetGet('email') ? $containerSession->offsetGet('email') : null )
//                :  $email;
            if ($provider) {
                if ($isNewProvider) {
                    if ($emailUser) {
                        $email = $emailUser;
                    }
                } else {
                    if ($containerSession->offsetGet('email')) {
                        $email = $containerSession->offsetGet('email');
                    }
                }
            } else {
                if ($emailUser) {
                    $email = $emailUser;
                }

            }


            if (isset($session1->userId)) {
                /** @var Request $request */

                $userProvider = $em->getRepository(UserProvider::class)->findOneBy(['userId' => $userId, 'provider' => $provider]);
                $userProvider->setUserId($session1->userId);


                if ($containerSession->isNewProvider) {
                    $user = $em->getRepository(User::class)->findOneBy(['id' => $userId]);
                    $em->remove($user);
                }
                $em->flush();
                $e->setIdentity($session1->userId);
                unset($session1->userId);
            }
            $userId = $e->getIdentity();
            //$email = $containerSession->offsetGet('email');// isset($containerSession->email) ? $containerSession->email : null;

            if (!empty($email)) {
                $emailObject = $em->getRepository(Email::class)->findOneBy(['email' => $email]);//$em->getRepository(Email::class)->findEmail($email);
                if (!empty($emailObject)) {
                    $userEmail = $em->getRepository(UserEmail::class)->findOneBy(['email' => $email]);
                    if (!empty($userEmail)) {
                        $userEmail->setUserId($userId);
                    } else {
                        $userEmail = new UserEmail();
                        $userEmail->setUserId($userId);
                        //$userEmail->setEmail($email);
                        $userEmail->setEmailSingle($emailObject);
                        $em->persist($userEmail);
                    }
                } else {
                    $emailObject = new Email();
                    $emailObject->setEmail($email);
                    $em->persist($emailObject);

                    $userEmail = new UserEmail();
                    $userEmail->setUserId($userId);
                    //$userEmail->setEmail($email);
                    $userEmail->setEmailSingle($emailObject);
                    $em->persist($userEmail);

                }

                $em->flush();
            }
            $containerSession->offsetUnset('email');
            $containerSession->offsetUnset('isNewProvider');
        });

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }




}
