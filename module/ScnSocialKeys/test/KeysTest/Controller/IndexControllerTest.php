<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 06.05.2017
 * Time: 10:10
 */

namespace Keys\Controller;

use Zend\Test\PHPUnit\Controller\AbstractControllerTestCase;

class IndexControllerTest extends AbstractControllerTestCase{

    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
            include '.\config\application.config.php'
        );
        parent::setUp();
    }

    public function testkeysActionAccessed()
    {
        $this->dispatch('/ru/user/key');
        $this->assertResponseStatusCode(200);
        $this->assertTrue(true);
        $this->assertEquals("r","r");
//        $this->assertModuleName('ScnSocialKeys');
//        $this->assertControllerName('ScnSocialKeys\Controller\Soc');
//        $this->assertControllerClass('SocController');
//        $this->assertMatchedRouteName('keys');
//        $this->assertActionName('keys');

    }




}
