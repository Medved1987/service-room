<?php
namespace ScnSocialKeys\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\View\Helper\AbstractHelper;

class Providers extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    public function __invoke()
    {
        $config = $this->getServiceLocator()->getServiceLocator()->get('Config');
        $provider = [];
        if(isset($config['scn-social-auth'])){
            foreach ($config['scn-social-auth'] as $key => $value){
                $line = explode("_", $key);
                foreach ($line as $item){
                    if($item == 'enabled'){
                        if($value){
                            $provider[] =  $line[0];
                        }
                    }
                }
            }
        }
        return $provider;
    }
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
}
