<?php

namespace ScnSocialKeys\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * An example entity that represents a role.
 *
 * @ORM\Entity
 * @ORM\Table(name="user_role")
 *
 * @author Tom Oram <tom@scl.co.uk>
 */
class Role
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="role_id",type="string", length=255, unique=true, nullable=true)
     */
    protected $roleId;

    /**
     * @var Role
     * @ORM\ManyToOne(targetEntity="ScnSocialKeys\Entity\Role")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="role_id", nullable=true)
     */
    protected $parent;

    /**
     * @return string
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * @param string $roleId
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;
    }

    /**
     * Get the parent role
     *
     * @return Role
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set the parent role.
     *
     * @param Role $role
     *
     * @return void
     */
    public function setParent(Role $parent)
    {
        $this->parent = $parent;
    }
}
