<?php
/**
 * ScnSocialAuth Module
 *
 * @category   ScnSocialAuth
 * @package    ScnSocialAuth_Service
 */

namespace ScnSocialKeys\Service;


use ScnSocialKeys\Controller\UserController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class UserControllerFactory implements FactoryInterface
{
//    public function createService(ServiceLocatorInterface $controllerManager)
//    {
//        $mapper = $controllerManager->getServiceLocator()->get('ScnSocialAuth-UserProviderMapper');
//        $moduleOptions = $controllerManager->getServiceLocator()->get('ScnSocialAuth-ModuleOptions');
//        $redirectCallback = $controllerManager->getServiceLocator()->get('zfcuser_redirect_callback');
//
//        $controller = new UserController($redirectCallback);
//        $controller->setMapper($mapper);
//        $controller->setOptions($moduleOptions);
//
////        try {
////          $hybridAuth = $controllerManager->getServiceLocator()->get('HybridAuth');
////          $controller->setHybridAuth($hybridAuth);
////        } catch (\Zend\ServiceManager\Exception\ServiceNotCreatedException $e) {
////          // This is likely the user cancelling login...
////        }
//
//        return $controller;
//    }

    public function createService(ServiceLocatorInterface $controllerManager)
    {
        /* @var ControllerManager $controllerManager*/
        $serviceManager = $controllerManager->getServiceLocator();

        /* @var RedirectCallback $redirectCallback */
        $redirectCallback = $serviceManager->get('zfcuser_redirect_callback');

        /* @var UserController $controller */
        $controller = new UserController($redirectCallback);
        $controller->setServiceLocator($serviceManager);

        $controller->setChangeEmailForm($serviceManager->get('zfcuser_change_email_form'));
        $controller->setOptions($serviceManager->get('zfcuser_module_options'));
        $controller->setChangePasswordForm($serviceManager->get('zfcuser_change_password_form'));
        $controller->setLoginForm($serviceManager->get('zfcuser_login_form'));
        $controller->setRegisterForm($serviceManager->get('zfcuser_register_form'));
        $controller->setUserService($serviceManager->get('zfcuser_user_service'));

        return $controller;
    }
}
