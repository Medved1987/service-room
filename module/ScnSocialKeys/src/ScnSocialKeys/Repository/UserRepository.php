<?php
namespace ScnSocialKeys\Repository;

use Doctrine\ORM\EntityRepository;
use Entities;
use ScnSocialKeys\Entity\User;
use ScnSocialKeys\Entity\UserKey;

class UserRepository extends EntityRepository
{
    public function isNewProvider($userId,$provider){
        $em = $this->getEntityManager();
        $queryBuilder = $em->createQueryBuilder();

        $queryBuilder->select('uk')
            ->from(User::class, 'uk')
            ->where('uk.id = :id')
            ->andWhere('uk.password LIKE  :provider')
            ->setParameter('id', $userId)
            ->setParameter('provider', $provider.'%');

        $result = $queryBuilder->getQuery()->getResult();

        return count($result);
    }




}