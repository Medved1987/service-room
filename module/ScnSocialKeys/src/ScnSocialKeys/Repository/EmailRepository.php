<?php
namespace ScnSocialKeys\Repository;

use Doctrine\ORM\EntityRepository;
use Entities;
use ScnSocialKeys\Entity\Email;
use ScnSocialKeys\Entity\UserKey;

class EmailRepository extends EntityRepository
{
    public function findEmail($email){
        $em = $this->getEntityManager();
        $queryBuilder = $em->createQueryBuilder();

        $queryBuilder->select('em')
            ->from(Email::class, 'em')
            ->where('em.email = :email')
            ->setParameter('email', $email);

        $result = $queryBuilder->getQuery()->getResult();

        return $result;
    }




}