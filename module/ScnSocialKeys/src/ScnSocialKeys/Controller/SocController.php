<?php
namespace ScnSocialKeys\Controller;

class SocController extends \ScnSocialAuth\Controller\UserController
{

    public function providerAuthenticateAction()
    {
        // Get the provider from the route
        $provider = $this->getEvent()->getRouteMatch()->getParam('provider');
        if (!in_array($provider, $this->getOptions()->getEnabledProviders())) {
            return $this->notFoundAction();
        }

        if (!$this->hybridAuth) {
            // This is likely user that cancelled login...
            return $this->redirect()->toRoute('zfcuser/login', ['lang' => $this->lang()]);
        }

        // For provider authentication, change the auth adapter in the ZfcUser Controller Plugin
        $this->zfcUserAuthentication()->setAuthAdapter($this->getServiceLocator()->get('ScnSocialAuth-AuthenticationAdapterChain'));

        // Adding the provider to request metadata to be used by HybridAuth adapter
        $this->getRequest()->setMetadata('provider', $provider);

        // Forward to the ZfcUser Authenticate action
        return $this->forward()->dispatch("\ScnSocialKeys\Controller\User", array('action' => 'authenticate'));
    }

    public function keysAction()
    {
        $providers = $this->getOptions()->getEnabledProviders();

        return ['providers'=>$providers];
    }


}
