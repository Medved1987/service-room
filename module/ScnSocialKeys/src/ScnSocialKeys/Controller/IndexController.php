<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ScnSocialKeys\Controller;

use ScnSocialKeys\Entity\User;
use ScnSocialKeys\Entity\UserKey;
use ScnSocialKeys\Entity\UserProvider;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{


    public function indexAction()
    {
        return;
    }

    public function deleteAction()
    {
        $provider = $this->params()->fromRoute('provider');
        $authService = $this->zfcUserAuthentication()->getAuthService();
        $socialId = $authService->getIdentity()->getId();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        if($authService->hasIdentity()){//->clearIdentity();

            $userProvider =  $em->getRepository(UserProvider::class)->findOneBy(['userId'=>$socialId,'provider'=>$provider]);
            if(!empty($userProvider)){

                $em->remove($userProvider);
                $em->flush();
                $userProvider =  $em->getRepository(UserProvider::class)->findBy(['userId'=>$socialId]);
                $user =  $em->getRepository(User::class)->findOneBy(['id'=>$socialId]);

                if (
                        empty($user->getEmail()) && (count($userProvider) < 1)
                ){
                    $em->remove($user);
                    $em->flush();

                    $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
                    $this->zfcUserAuthentication()->getAuthAdapter()->logoutAdapters();
                    $this->zfcUserAuthentication()->getAuthService()->clearIdentity();
                    return $this->redirect()->toRoute('zfcuser');
                }


            }
            return $this->redirect()->toRoute('keys');
        }else{
            return $this->redirect()->toRoute('zfcuser');
        }
    }


}
