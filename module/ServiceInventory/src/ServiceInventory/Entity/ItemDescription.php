<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="ServiceInventory\Repository\ItemDescriptionRepository")
 * @ORM\Table(name="item_description")
 */
class ItemDescription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;

    /**
     * @ORM\Column(name="description")
     */
    private $description;


    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\ItemStatus", inversedBy="itemDescription", cascade={"persist"})
     * @ORM\JoinColumn(name="item_status_id", referencedColumnName="id")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\Item", inversedBy="itemDescription", cascade={"persist"})
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $items;


    /**
     * @ORM\Column(name="date" , nullable=true,type="datetime")
     */
    private $date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        $status->addItemDescription($this);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /*
     * @return \Inventory\Entity\Items
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param \ServiceInventory\Entity\Item $items
     */
    public function setItems($items)
    {
        $this->items = $items;
        $items->addItemDescription($this);
    }

}