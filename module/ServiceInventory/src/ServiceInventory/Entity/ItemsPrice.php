<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="item_price")
 */
class ItemsPrice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\Item", inversedBy="itemsPrice", cascade={"persist"})
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $items;

    /**
     * @ORM\Column(name="price")
     */
    private $price;
    /**
     * @ORM\Column(name="active")
     */
    private $active = 1;

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }



    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\PriceSymbol", inversedBy="itemsPrice")
     * @ORM\JoinColumn(name="price_symbol", referencedColumnName="symbol")
     */
    private $priceSymbol;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /*
     * @return \ServiceInventory\Entity\Items
     */
    public function getPriceSymbol()
    {
        return $this->priceSymbol;
    }

    /**
     * @param \ServiceInventory\Entity\PriceSymbol $priceSymbol
     */
    public function setPriceSymbol($priceSymbol)
    {
        $this->priceSymbol = $priceSymbol;
        $priceSymbol->addItemsPrice($this);
    }

    /*
     * @return \ServiceInventory\Entity\Items
     */
    public function getItems()
    {
        return $this->items;
    }
    public function setItems($items)
    {
        $this->items = $items;
        $items->addItemsPrice($this);
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }




}