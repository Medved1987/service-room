<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="computer_user")
 */
class ComputerUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;

    /**
     * @ORM\Column(name="user_id")
     */
    private $userId;
    /**
     * @ORM\Column(name="computer_id")
     */
    private $computerId;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getComputerId()
    {
        return $this->computerId;
    }

    /**
     * @param mixed $computerId
     */
    public function setComputerId($computerId)
    {
        $this->computerId = $computerId;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

}