<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     */
    private $id;


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @ORM\ManyToMany(targetEntity="\ServiceInventory\Entity\Item", mappedBy="user")
     */
    private $items;

    /**
     * @ORM\ManyToMany(targetEntity="\ServiceInventory\Entity\Computer", mappedBy="user")
     */
    private $computer;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->computer = new ArrayCollection();
    }


    public function getComputer()
    {
        return $this->computer;
    }

    public function addComputer($computer)
    {
        $this->computer[] = $computer;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function addItems($items)
    {
        $this->items[] = $items;
    }


}