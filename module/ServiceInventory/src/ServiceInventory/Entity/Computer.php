<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="ServiceInventory\Repository\ComputerRepository")
 * @ORM\Table(name="computer")
 */
class Computer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;

    /**
     * @ORM\Column(name="name")
     */
    private $name;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\ComputerItem", mappedBy="computer")
     * @ORM\JoinColumn(name="id", referencedColumnName="computer_id")
     */
    protected $computerItem;

    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\ComputerFoto", mappedBy="computer")
     * @ORM\JoinColumn(name="id", referencedColumnName="computer_id")
     */
    protected $computerFoto;

    /**
     * @ORM\ManyToMany(targetEntity="\ServiceInventory\Entity\User", inversedBy="computer")
     * @ORM\JoinTable(name="computer_user",
     *      joinColumns={@ORM\JoinColumn(name="computer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    protected $user;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->computerItem = new ArrayCollection();
        $this->computerFoto = new ArrayCollection();
    }



    public function getComputerItem()
    {
        return $this->computerItem;
    }


    public function addComputerItem($computerItem)
    {
        $this->computerItem[] = $computerItem;
    }
    public function getComputerFoto()
    {
        return $this->computerFoto;
    }


    public function addComputerFoto($computerFoto)
    {
        $this->computerFoto[] = $computerFoto;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function addUser($user)
    {
        $this->user[] = $user;
    }

    public function removeUserAssociation($user)
    {
        $this->user->removeElement($user);
    }


}