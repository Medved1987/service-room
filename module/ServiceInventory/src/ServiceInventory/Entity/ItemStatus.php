<?php
namespace ServiceInventory\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ServiceInventory\Repository\ItemStatusRepository")
 * @ORM\Table(name="item_status")
 */
class ItemStatus
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;


    /**
     * @ORM\Column(name="status")
     */
    private $status;

    /**
     * @ORM\Column(name="is_vendor")
     */
    private $vendor = 0;
    /**
     * @ORM\Column
     */
    private $color = 0;
    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\ItemDescription", mappedBy="status", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="status_id")
     */
    private $itemDescription;

    /**
     * ItemStatus constructor.
     * @param $itemDescription
     */
    public function __construct()
    {
        $this->itemDescription = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param mixed $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getItemDescription()
    {
        return $this->itemDescription;
    }

    /**
     * @param mixed $itemDescription
     */
    public function addItemDescription($itemDescription)
    {
        $this->itemDescription[] = $itemDescription;
    }



}