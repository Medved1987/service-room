<?php
namespace ServiceInventory\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ServiceInventory\Repository\TypeItemRepository;

/**
 * @ORM\Entity(repositoryClass="ServiceInventory\Repository\TypeItemRepository")
 * @ORM\Table(name="type_item")
 */
class TypeItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;

    /**
     * @ORM\Column(name="type",unique=true)
     */
    private $type;


    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\ItemsSpec", mappedBy="typeItem")
     * @ORM\JoinColumn(name="type", referencedColumnName="type_item_id")
     */
    private $itemsSpec;

    public function __construct()
    {
        $this->itemsSpec = new ArrayCollection();
    }

    public function getItemsSpec()
    {
        return $this->itemsSpec;
    }
    public function addItemsSpec($itemsSpec)
    {
        $this->itemsSpec[] = $itemsSpec;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }


}