<?php
/**
 * Created by PhpStorm.
 * User: user1
 * Date: 29.03.2017
 * Time: 10:18
 */

namespace ServiceInventory\Entity;


use Doctrine\ORM\EntityManagerInterface;

interface EntityManagerAwareInterface
{
    public function setEntityManager(EntityManagerInterface $entityManager);
    public function getEntityManager();
}