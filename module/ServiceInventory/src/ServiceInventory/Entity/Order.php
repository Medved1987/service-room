<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ServiceInventory\Entity\Items;
use ServiceInventory\Repository\OrderRepository;

/**
 * @ORM\Entity(repositoryClass="ServiceInventory\Repository\OrderRepository")
 * @ORM\Table(name="`order`",options={"engine":"InnoDB"})
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="name")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\Item", mappedBy="orders")
     * @ORM\JoinColumn(name="id", referencedColumnName="order_id")
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $items
     */
    public function addItems($items)
    {
        $this->items[] = $items;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }


}