<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="spec_foto")
 */
class SpecFoto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;
    /**
     * @ORM\Column(name="url")
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\ItemsSpec", inversedBy="specFoto", cascade={"persist"})
     * @ORM\JoinColumn(name="item_spec_id", referencedColumnName="id")
     */
    private $itemSpec;


    /*
     * @return \Inventory\Entity\Computer
     */
    public function getItemSpec()
    {
        return $this->itemSpec;
    }

    /**
     * @param \ServiceInventory\Entity\Computer $computer
     */
    public function setItemSpec($itemSpec)
    {
        $this->itemSpec = $itemSpec;
        $itemSpec->addSpecFoto($this);
    }



    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setId($id)
    {
        $this->id = $id;
    }



}