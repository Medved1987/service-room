<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ServiceInventory\Repository\ComputerItemRepository")
 * @ORM\Table(name="computer_item")
 */
class ComputerItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;

//    private $computerId;
    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\Item", inversedBy="computerItem", cascade={"persist"})
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\Computer", inversedBy="computerItem", cascade={"persist"})
     * @ORM\JoinColumn(name="computer_id", referencedColumnName="id")
     */
    private $computer;


    /*
     * @return \Inventory\Entity\Computer
     */
    public function getComputer()
    {
        return $this->computer;
    }

    /**
     * @param \ServiceInventory\Entity\Computer $computer
     */
    public function setComputer($computer)
    {
        $this->computer = $computer;
        $computer->addComputerItem($this);
    }

    /*
     * @return \Inventory\Entity\Items
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param \ServiceInventory\Entity\Item $items
     */
    public function setItems($items)
    {
        $this->items = $items;
        $items->addComputerItem($this);
    }



    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }



}