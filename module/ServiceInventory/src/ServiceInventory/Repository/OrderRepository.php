<?php
namespace ServiceInventory\Repository;

use ServiceInventory\Entity\Order;
use Doctrine\ORM\EntityRepository;
use Entities;


class OrderRepository extends EntityRepository
{
    public function getOrders(){
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('o')
            ->from(Order::class, 'o')
            ->orderBy('o.date', 'DESC');

        return $queryBuilder->getQuery();
    }
}