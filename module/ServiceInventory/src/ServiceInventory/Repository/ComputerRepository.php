<?php
namespace ServiceInventory\Repository;

use ServiceInventory\Entity\Computer;
use ServiceInventory\Entity\ComputerItem;
use ServiceInventory\Entity\Item;
use Doctrine\ORM\EntityRepository;
use Entities;


class ComputerRepository extends EntityRepository
{
    public function getComputers(){
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('c')
            ->from(Computer::class, 'c')
            ->orderBy('c.name', 'DESC');

        return $queryBuilder->getQuery();
    }

    public function getComputersFree(){
        $entityManager = $this->getEntityManager();
        $qb = $entityManager->createQueryBuilder();

        $qb->select('c')
            ->from(Computer::class, 'c')
            ->where('c.user  is empty')
        ;
        $computer = $qb->getQuery()->getResult();

        return $computer;

    }

    public function findComputersUser($userId){
        $entityManager = $this->getEntityManager();
        $qb = $entityManager->createQueryBuilder();

        $qb->select('c')
            ->from(Computer::class, 'c')
            ->innerJoin(ComputerItem::class, 'ci', 'WITH','ci.computer = c.id')
            ->innerJoin(Item::class, 'i', 'WITH','i.id = ci.items AND i.finishDate IS NULL')
            ->where(':userId  MEMBER OF c.user')
            ->setParameter('userId',$userId)
        ;
        $computer = $qb->getQuery()->getResult();

        return $computer;

    }
}