<?php
namespace ServiceInventory\Repository;

use ServiceInventory\Entity\ComputerItem;
use ServiceInventory\Entity\Item;
use ServiceInventory\Entity\ItemDescription;
use ServiceInventory\Entity\Items;
use Doctrine\ORM\EntityRepository;
use Entities;
use Doctrine\Common\Util\Debug;

class ItemsRepository extends EntityRepository
{
    public function getItemsFree(){
        $entityManager = $this->getEntityManager();
        $qb = $entityManager->createQueryBuilder();

        $qb->select('i')
            ->from(Item::class, 'i');
        $items = $qb->getQuery()->getResult();
        $result = [];

        foreach ($items as $item){
            $qb = $entityManager->createQueryBuilder();
            $qb->select('id')
                ->from(ItemDescription::class, 'id')
                ->where('id.items = :id')
                ->orderBy('id.date','DESC')
                ->setParameter('id',$item->getId())
                ->setMaxResults(1)
            ;
            $r = $qb->getQuery()->getSingleResult();
            if($r->getStatus()->getId() == 2){
                $result[] = $item;
            }
        }

        return $result;


    }
    public function findItemsUser($userId){
        $entityManager = $this->getEntityManager();
        $qb = $entityManager->createQueryBuilder();

        $qb->select('i')
            ->from(Item::class, 'i')
            ->where(':userId  MEMBER OF i.user AND i.finishDate IS NULL')
            ->setParameter('userId',$userId)
        ;
        $computer = $qb->getQuery()->getResult();

        return $computer;

    }
    public function getItems(){
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('i')
            ->from(Item::class, 'i')
            ->orderBy('i.id', 'DESC');

        return $queryBuilder->getQuery();
    }

}