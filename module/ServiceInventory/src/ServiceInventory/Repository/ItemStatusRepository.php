<?php
namespace ServiceInventory\Repository;

use ServiceInventory\Entity\ItemStatus;
use Doctrine\ORM\EntityRepository;
use Entities;


class ItemStatusRepository extends EntityRepository
{
    public function findStatus(){
        $result = [];
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('i')
            ->from(ItemStatus::class, 'i');

        $itemStatus = $queryBuilder->getQuery()->getArrayResult();
        if($itemStatus) {
            foreach ($itemStatus as $value) {
                $result[$value['id']] = $value['status'];
            }
        }

        return $result;

    }



    }