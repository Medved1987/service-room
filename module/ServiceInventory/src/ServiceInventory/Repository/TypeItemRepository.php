<?php
namespace ServiceInventory\Repository;

use ServiceInventory\Entity\TypeItem;
use Doctrine\ORM\EntityRepository;
use Entities;

class TypeItemRepository extends EntityRepository
{
    public function findTypeItems(){
        $result =[];
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('ti')
            ->from(TypeItem::class, 'ti')
            ->groupBy('ti.type')
            ->orderBy('ti.type', 'ASC');

        $types = $queryBuilder->getQuery()->getArrayResult();
        if($types) {
            foreach ($types as $type) {
                $result[$type['id']] = $type['type'];
            }
        }

        return $result;
    }


}