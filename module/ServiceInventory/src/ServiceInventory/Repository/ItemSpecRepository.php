<?php
namespace ServiceInventory\Repository;

use ServiceInventory\Entity\ItemsSpec;
use Doctrine\ORM\EntityRepository;
use Entities;

class ItemSpecRepository extends EntityRepository
{

    public function findItemSpec($name){
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('ti')
            ->from(ItemsSpec::class, 'ti')
            ->groupBy('ti.name')
            ->orderBy('ti.name', 'ASC')
            ->where('ti.name LIKE :name')
            ->setParameter('name', $name.'%');

        $types = $queryBuilder->getQuery()->getArrayResult();


        return $types;
    }


}