<?php
namespace ServiceInventory\Repository;

use ServiceInventory\Entity\ComputerItem;
use Doctrine\ORM\EntityRepository;
use Entities;

class ComputerItemRepository extends EntityRepository
{


    public function isItemsFree($arr){
        $entityManager = $this->getEntityManager();
        $qb = $entityManager->createQueryBuilder();

        $qb->select('ci')
            ->from(ComputerItem::class, 'ci')
            ->where('ci.items IN (:id)')
            ->setParameter('id',$arr);
        return empty($qb->getQuery()->getArrayResult()) ? true : false;
    }

    public function getCountComputer($id){
        $entityManager = $this->getEntityManager();
        $qb = $entityManager->createQueryBuilder();

        $qb->select('count(ci)')
            ->from(ComputerItem::class, 'ci')
            ->where('ci.computer = (:id)')
            ->setParameter('id',$id);
        return $qb->getQuery()->getSingleScalarResult();
    }

}