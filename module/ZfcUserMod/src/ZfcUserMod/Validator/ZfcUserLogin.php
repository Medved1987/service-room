<?php
namespace ZfcUserMod\Validator;

use Zend\Validator\AbstractValidator;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use HtUserRegistration\Mapper\UserRegistrationMapper;
use ZfcUser\Mapper\User as UserMapper;
use ZfcUser\Entity\UserInterface;
use HtUserRegistration\Entity\UserRegistrationInterface;
use Zend\Debug\Debug;
class ZfcUserLogin extends AbstractValidator
{
    protected $serviceLocator;
    
    const EMAIL_NOT_FOUND   = 'email_not_found';
    const EMAIL_NOT_AUTH    = 'email_not_auth';
    const NOT_INIT          = 'not_init Mappers';
//     const XFLOAT = 'xfloat';

    protected $messageTemplates = array(
        self::EMAIL_NOT_FOUND => "Email Address not registration yet",//Email Address not registration yet
        self::EMAIL_NOT_AUTH => "Email Address not verified yet"
//         self::XFLOAT => "'%value%' is not a XFLOAT"
    );

    public function isValid($value)
    {
        $this->setValue($value);
        
        /*
         * Получаем 2 маппера и сравниваем
         * */
        /*
         * User
         * */
        
        $zfcuser_user_mapper = $this->getServiceLocator()->get('zfcuser_user_mapper');
//         echo get_class($zfcuser_user_mapper);
        
        if ($zfcuser_user_mapper instanceof UserMapper) {
            $User = $zfcuser_user_mapper->findByEmail($value);
            
            if ($User instanceof UserInterface) {
                
            } else {
                $this->error(self::EMAIL_NOT_FOUND);
                return false;//Нет пользователея в системе
            }
        }
        
        /*
         * Reg
         * */
        $UserRegistrationMapper = $this->getServiceLocator()->get('HtUserRegistration\UserRegistrationMapper');
//         echo get_class($UserRegistrationMapper);
        
        if ($UserRegistrationMapper instanceof UserRegistrationMapper) {
            $UserRegistration = $UserRegistrationMapper->findByUser($User);
            
            if ($UserRegistration instanceof UserRegistrationInterface) {
                
//                 Debug::dump($UserRegistration);
                
                if (
                   $UserRegistration->isResponded()
                ) {
                    return true; //пользователь подтвердил Email
                } else {
                    $this->error(self::EMAIL_NOT_AUTH);
                    return false;//пользователь не подтвердил Email
                }
            } 
        }//if
        
        $this->error(self::NOT_INIT);
        return false;//что то вообще не то

    }
    
    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
    */
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function setServiceLocator(ServiceLocatorInterface $ServiceLocator) {
         $this->serviceLocator = $ServiceLocator;
         return $this;
    }
}

?>