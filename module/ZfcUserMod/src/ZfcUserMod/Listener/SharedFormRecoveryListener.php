<?php
namespace ZfcUserMod\Listener;

use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\EventInterface;
use ZfcUser\Form\LoginFilter;
use ZfcUser\Form\Login as LoginForm;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use HtUserRegistration\Service\UserRegistrationService;
use HtUserRegistration\Mailer\MailerInterface;
class SharedFormRecoveryListener implements SharedListenerAggregateInterface, ServiceLocatorAwareInterface
{
    protected 
        $listeners = array(),
        $serviceLocator;
    
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(
            'ZfcUser\Service\User',
            'recoveryEmail',
            array($this, 'recoveryEmail'),
            100
        );
    }
    
    public function detachShared(SharedEventManagerInterface  $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    /*
     * Проверка данных, по какой причине мы не можем залогиниться
     * Вместо FlashMessager
     * */
    function recoveryEmail(EventInterface $e){
        $ZfcUserService = $e->getTarget();
//         echo get_class($ZfcUserService).'<br/>';
        $UserEntity             = $e->getParam('user');
        $UserRegistrationEntity = $e->getParam('userRegistration');
//         echo get_class($UserEntity).'<br/>';
//         echo get_class($UserRegistrationEntity).'<br/>';
        
        /*
         * Отправить почту верификации
         * */
        $UserRegistrationMailer = $this->getServiceLocator()->get('HtUserRegistration\Mailer\Mailer');
        
        if ($UserRegistrationMailer instanceof MailerInterface) {
            $UserRegistrationMailer->sendVerificationEmail($UserRegistrationEntity);
        }
        
        
        
        
        
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
}

?>