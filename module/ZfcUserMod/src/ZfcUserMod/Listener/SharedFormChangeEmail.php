<?php
namespace ZfcUserMod\Listener;

use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\EventInterface;

class SharedFormChangeEmail implements SharedListenerAggregateInterface
{
    
    public function detachShared(SharedEventManagerInterface  $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        /*
         * authenticate.pre
         * authenticate
         * authenticate.success
         * authenticate.fail
         * 
         * */
        $this->listeners[] = $events->attach(
             'ZfcUser\Form\ChangeEmail',
             'init', 
             array($this, 'success'), 
             100
         );
        
    }
    
    function success(EventInterface $e) {
        $changeEmailObject = $e->getTarget();
        $changeEmailObject->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Change email',
            ),
        ));
    }
    
    function fail($e) {
        echo 'fail<hr/>';
    }
}