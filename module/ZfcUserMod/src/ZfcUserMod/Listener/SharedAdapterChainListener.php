<?php
namespace ZfcUserMod\Listener;

use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\EventInterface;
use ZfcUser\Form\LoginFilter;
use ZfcUser\Form\Login as LoginForm;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
class SharedAdapterChainListener implements SharedListenerAggregateInterface, ServiceLocatorAwareInterface
{
    protected 
        $listeners = array(),
        $serviceLocator;
    
    public function detachShared(SharedEventManagerInterface  $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        /*
         * authenticate.pre
         * authenticate
         * authenticate.success
         * authenticate.fail
         * 
         * */
        $this->listeners[] = $events->attach(
             'ZfcUser\Authentication\Adapter\AdapterChain', 
             'authenticate.success', 
             array($this, 'success'), 
             100
         );
        
        $this->listeners[] = $events->attach(
            'ZfcUser\Authentication\Adapter\AdapterChain',
            'authenticate.fail',
            array($this, 'fail'),
            100
        );
        
    }
    
    function success($e) {
        echo 'success<hr/>';
    }
    
    function fail($e) {
        echo 'fail<hr/>';
    }
    
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
}

?>