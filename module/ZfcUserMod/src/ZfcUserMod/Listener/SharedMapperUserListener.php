<?php
namespace ZfcUserMod\Listener;

use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\EventInterface;
class SharedMapperUserListener implements SharedListenerAggregateInterface
{
    protected $listeners = array();
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(
             'ZfcUser\Mapper\User', 
             'find', 
             array($this, 'find'), 
             100
         );
        
    }
    
    public function detachShared(SharedEventManagerInterface  $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    function find(){
//         echo 'find :)';
    }
    
}

?>