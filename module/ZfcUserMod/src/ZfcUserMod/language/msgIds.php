<?php
/**
 * Ids for poedit
 * @method translate()
 */
//Login
translate('Back to login');
translate('The user has been created successfully An email has been sent to');
translate('It contains an activation link you must click to activate your account');
translate('Enter');
translate('Login with Facebook');
translate('Login with Email');
translate('Email');
translate('Password');
translate('Remember me');
translate('Forgot password');
translate('Not registered?');
translate('Register');

//Register
translate('Registration is disabled');
translate('Password Verify');

//Change password and Email
translate('Change Password for %s');
translate('Password changed successfully.');
translate('Unable to update your password. Please try again.');
translate('Enter current password');
translate('Enter new password');
translate('Re-enter new password');
translate('Change Email for %s');
translate('Email address changed successfully');
translate('Unable to update your email address. Please try again.');
translate('Enter new Email');
translate('Confirm new Email');

//forgot email
translate('You have received this email because you initiated to reset your password with us. To continue with the process, please click the link below:');

//forgot password changed 
translate('Your password has been changed.');
translate('Confirm new password');
translate('A reminder has been sent to your e-mail address %s.');
translate('You can now login using your new password.');
translate('Back to login.');

//mail verify
translate('Set your account password');
translate('Email Address Verification');
translate('Please click on the following link to verify your email address');

//recovery
translate('Re-send email verification');
translate('Go to login');

//translate('Username');
//translate('Email');
//translate('New Email');
//translate('Verify New Email');
//translate('Display Name');
//translate('Password');
//translate('Password Verify');
//translate('Current Password');
//translate('Verify New Password');
//translate('New Password');
//translate('Please type the following text');
//translate('Submit');
//
//translate('Sign In');
translate('Register');
translate('Register with Facebook');
translate('Register with Email');
//translate("No record matching the input was found");
//translate("A record matching the input was found");
//translate("Authentication failed. Please try again.");
////
translate("Not registered?");
//translate("Sign up!");
//translate("Change Email for %s");
//translate("Email address changed successfully.");
//translate("Unable to update your email address. Please try again.");
//translate("Change Password for %s");
//translate("Password changed successfully.");
//translate("Unable to update your password. Please try again.");
translate("Sign Out");
translate('An error occured during verification of email.');

//ht-user-registration
translate("Email Address not verified yet");
translate("Email Address not registration yet");

translate('It contains an activation link you must click to activate your account');