<?php

namespace ZfcUserMod\Form;

use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use ZfcUser\Options\RegistrationOptionsInterface;
use ZfcUser\Validator\RecordExists;

class Register extends \ZfcUser\Form\Register
{
    public function __construct($name, RegistrationOptionsInterface $options)
    {
        parent::__construct($name, $options);
        
        $this->add(
            [
                'type'       => 'hidden',
                'name'       => 'account_type',
                'attributes' => [
                    'value' => 'user-client',
                ],
            ]
        );
    }
}
