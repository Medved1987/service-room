<?php

namespace ZfcUserMod\Form;

use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcUserMod\Validator\RecordExists;

class RegisterFilter extends InputFilter
{
    use ServiceLocatorAwareTrait;
    
    public function __construct(ServiceLocatorInterface $serviceLocator = null)
    {
        $this->setServiceLocator($serviceLocator);
    
        $this->add(
            [
                'name'       => 'email',
                'required'   => true,
                'filters'    => [
                    [
                        'name' => 'StringTrim',
                    ],
                ],
                'validators' => [
                    [
                        'name' => 'EmailAddress'
                    ],
                
                    new RecordExists(
                        [
                            'mapper' => $serviceLocator->get('zfcuser_user_mapper'),
                            'key'    => 'email'
                        ]
                    ),
                ]
            ]
        );
    
    
        $this->add(
            [
                'name'       => 'password',
                'required'   => true,
                'filters'    => [['name' => 'StringTrim']],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 6,
                        ],
                    ],
                ],
            ]
        );
    
        $this->add(
            [
                'name'       => 'passwordVerify',
                'required'   => true,
                'filters'    => [['name' => 'StringTrim']],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 6,
                        ],
                    ],
                    [
                        'name'    => 'Identical',
                        'options' => [
                            'token' => 'password',
                        ],
                    ],
                ],
            ]
        );
    }
}
