<?php
namespace ZfcUserMod\Form;

use Zend\Form\Form;
class Recovery extends Form
{
    function __construct($name = null)
    {
        parent::__construct('RecoveryForm');
        
        $this->add(array(
            'name' => 'email',
            'type' => 'Email',
            'options' => array(
                'label' => 'Email',
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'save',
                'id' => 'submitbutton',
            ),
        ));
    }
}

?>