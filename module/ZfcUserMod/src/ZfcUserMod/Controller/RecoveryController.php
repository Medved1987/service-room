<?php
namespace ZfcUserMod\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use ZfcUserMod\Form\Recovery;
use Zend\Debug\Debug;
use HtUserRegistration\Mapper\UserRegistrationMapper;
use HtUserRegistration\Entity\UserRegistrationInterface;
use ZfcUser\Service\User as ZfcUserService;
class RecoveryController extends AbstractActionController
{
    function indexAction() {
        
        $form = new Recovery();
        
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData( $data);
            
            if ($form->isValid()) {
//                 Debug::dump( $form->getData());
                
                $zfcuser_user_mapper    = $this->getServiceLocator()->get('zfcuser_user_mapper');
                $UserEntity             = $zfcuser_user_mapper->findByEmail( $data->email);
                //Debug::dump( $User);
                if ( $UserEntity) {

                    try {
                        $UserRegistrationMapper = $this->getServiceLocator()->get('HtUserRegistration\UserRegistrationMapper');
                        //         echo get_class($UserRegistrationMapper);
                        
                        if ($UserRegistrationMapper instanceof UserRegistrationMapper) {
                            $UserRegistration = $UserRegistrationMapper->findByUser($UserEntity);

                            if ($UserRegistration instanceof UserRegistrationInterface) {
                                
                                /*
                                 * Отправка почты
                                 * */
//                                 Debug::dump($UserRegistration);
                        
                                if (
                                    $UserRegistration->isResponded()
                                ) {//высланно
                                    $this->flashMessenger()->addSuccessMessage('Пользователь уже авторизован');
                                } else {
                                    //SEND
                                    $this->flashMessenger()->addSuccessMessage('Письмо выслано на почту');
                                    $ZfcUserService = $this->getServiceLocator()->get('zfcuser_user_service');
                                    
                                    if ($ZfcUserService instanceof ZfcUserService) {
                                        
                                        $ZfcUserService->getEventManager()->trigger(
                                            'recoveryEmail', 
                                            $this, 
                                            array(
                                                'user' => $UserEntity,
                                                'userRegistration' => $UserRegistration
                                            ), function ($r) {
//                                                 echo 'XXX: '.get_class($r);
                                            }
                                        );//trigger
                                    }
                                }
                            }
                        }//if
                        
                    } catch (\Exception $e ) {
                        $this->flashMessenger()->addErrorMessage('Запрос произошел с ошибкой');
                    }//try
                    
                } else {//valid
                    $this->flashMessenger()->addErrorMessage('Почтовый адрес не существует');
                }
                
                $this->redirect()->refresh();
            }//if valid date
        }//if post
        
        return array(
            'form' => $form
        );
    }
}

