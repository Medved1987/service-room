<?php
/**
 * BjyAuthorize Module (https://github.com/bjyoungblood/BjyAuthorize)
 *
 * @link https://github.com/bjyoungblood/BjyAuthorize for the canonical source repository
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */

namespace BjyAuthorizeMod\Provider\Identity;

use Zend\Db\Sql\Sql;
use BjyAuthorize\Provider\Identity\ZfcUserZendDb;

/**
 * Identity provider based on {@see \Zend\Db\Adapter\Adapter}
 *
 * @author Ben Youngblood <bx.youngblood@gmail.com>
 */
class ZfcUserZendDb extends ZfcUserZendDb
{

     public function getIdentityRoles()
    {
        $authService = $this->userService->getAuthService();

        if (! $authService->hasIdentity()) {
            return array($this->getDefaultRole());
        }

        // get roles associated with the logged in user
        $sql    = new Sql($this->adapter);
//         $select = $sql->select()->from($this->tableName);
//         $where  = new Where();

//         $where->equalTo('user_id', $authService->getIdentity()->getId());

//         $results = $sql->prepareStatementForSqlObject($select->where($where))->execute();
        $select = $sql->select();
        $select
            ->columns(array('role_id' => 'role_id'))
            ->from(array('roles' => 'user_role'))
            ->join(array('linker' => $this->tableName), 'roles.id = linker.role_id', array()) 
            ->where(array('linker.user_id = ?' => $authService->getIdentity()->getId()));
        
        echo $sql->getSqlStringForSqlObject($select); die;
        $results = $sql->prepareStatementForSqlObject($select)->execute();
        
        $roles     = array();

        foreach ($results as $i) {
            $roles[] = $i['role_id'];
        }

        return $roles;
    }

}
