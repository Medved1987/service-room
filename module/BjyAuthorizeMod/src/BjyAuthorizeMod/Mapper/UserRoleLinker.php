<?php
namespace BjyAuthorizeMod\Mapper;

use ZfcBaseMod\Mapper\AbstractDbMapperWithEntity;
class UserRoleLinker extends AbstractDbMapperWithEntity
{
    protected $tableName = 'user_role_linker';
}
