<?php
namespace BjyAuthorizeMod\Listener;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
class RegistrationListener implements ListenerAggregateInterface
{
    protected $listeners = array();
    
    function attach($events) {
        
    }
    
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
}

?>