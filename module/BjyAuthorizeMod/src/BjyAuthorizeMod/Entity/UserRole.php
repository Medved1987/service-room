<?php
namespace BjyAuthorizeMod\Entity;

class UserRole
{
    protected 
        $id, 
        $roleId,
        $isDefault,
        $parentId;
 /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }

 /**
     * @return the $roleId
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

 /**
     * @return the $isDefault
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

 /**
     * @return the $parentId
     */
    public function getParentId()
    {
        return $this->parentId;
    }

 /**
     * @param field_type $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

 /**
     * @param field_type $roleId
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;
    }

 /**
     * @param field_type $isDefault
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;
    }

 /**
     * @param field_type $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

}

?>