<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/BjyAuthorizeMod for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace BjyAuthorizeMod;

use BjyAuthorize\Exception\UnAuthorizedException;
use BjyAuthorize\Guard\Controller;
use Zend\Http\PhpEnvironment\Response;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\Application;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use BjyAuthorizeMod\Mapper\UserRole as UserRoleMapper;
use BjyAuthorizeMod\Entity\UserRole as UserRoleEntity;
use BjyAuthorizeMod\Mapper\UserRoleLinker as UserRoleLinkerMapper;
use BjyAuthorizeMod\Entity\UserRoleLinker as UserRoleLinkerEntity;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\EventManager\SharedEventManager;
use Zend\View\Model\ViewModel;

class Module implements AutoloaderProviderInterface, ServiceProviderInterface
{
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . '/autoload_classmap.php',
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ],
            ],
        ];
    }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getServiceConfig()
    {
        return [
            'invokables' => [
                'BjyAuthorizeMod\Listener\SharedFormLoginListener'
                                            => 'BjyAuthorizeMod\Listener\SharedFormLoginListener',
                
                'BjyAuthorizeMod\Listener\SharedMapperUserListener'
                                            => 'BjyAuthorizeMod\Listener\SharedMapperUserListener',
                
                'BjyAuthorizeMod\Listener\SharedFormRegistrationListener'
                                            => 'BjyAuthorizeMod\Listener\SharedFormRegistrationListener',
            ],
            
            'factories' => [
                'BjyAuthorizeMod\Provider\Identity\ZfcUserZendDb'
                                            => 'BjyAuthorizeMod\Service\ZfcUserZendDbIdentityProviderServiceFactory',
                
                //Role
                'BjyAuthorizeMod\Mapper\UserRole'       => function (ServiceLocatorInterface $SL) {
                    $UserRoleMapper = new UserRoleMapper();
                    $UserRoleMapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                    $UserRoleMapper->setEntityPrototype(new UserRoleEntity());
                    $UserRoleMapper->setHydrator(new ClassMethods());
                    
                    return $UserRoleMapper;
                },
                //Linker
                'BjyAuthorizeMod\Mapper\UserRoleLinker' => function (ServiceLocatorInterface $SL) {
                    $UserRoleMapperLinker = new UserRoleLinkerMapper();
                    $UserRoleMapperLinker->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                    $UserRoleMapperLinker->setEntityPrototype(new UserRoleLinkerEntity());
                    $UserRoleMapperLinker->setHydrator(new ClassMethods());
                    
                    return $UserRoleMapperLinker;
                },
            ],
        ];
    }
    
    public function onBootstrap(MvcEvent $e)
    {
        $ServiceLocator = $e->getApplication()->getServiceManager();
        $SEM = $e->getApplication()->getEventManager()->getSharedManager();
    
        
        /* перенаправляем на нормальную страницу с запретом доступа. */
//        /** @var EventManager $eventManager */
//        $eventManager = $e->getApplication()->getEventManager();
        $SEM->attach(Application::class, MvcEvent::EVENT_DISPATCH_ERROR, function (MvcEvent $e) {
            if (Controller::ERROR === $e->getError()
                && $e->getParam('exception') instanceof UnAuthorizedException
            ) {
                $response = $e->getResponse() ?? new Response();
                
                if (stripos($e->getRouteMatch()->getMatchedRouteName(), 'localization') !== false) {
                    $layout = new ViewModel();
                    $layout->setTemplate('layout/uber');
                } else {
                    $layout = new ViewModel();
                    $layout->setTemplate('layout/layout');
                }
                
                $model = new ViewModel();
                $model->setTemplate('error/permission-denied');
                $layout->addChild($model);
                
                $e->getViewModel()->addChild($layout);
                $response->setStatusCode(403);
                $e->setResponse($response);
                $e->stopPropagation();
            }
        }, 1000);
        
        if ($SEM instanceof SharedEventManager) {
            //Aggregate
            $SEM->attachAggregate($ServiceLocator->get('BjyAuthorizeMod\Listener\SharedFormLoginListener'));
            $SEM->attachAggregate($ServiceLocator->get('BjyAuthorizeMod\Listener\SharedMapperUserListener'));
            $SEM->attachAggregate($ServiceLocator->get('BjyAuthorizeMod\Listener\SharedFormRegistrationListener'));
            /*
             *
             * */
//             $SEM->attach(
//                 'ZfcUser\Form\ChangeEmail',
//                 'init',
//                 function (EventInterface $e) {
//                     echo $e->getName().get_class($e->getTarget());
//                 }
//             );

//             $SEM->attach(
//                 'ZfcUser\Form\ChangePassword',
//                 'init',
//                 function (EventInterface $e) {
//                     echo $e->getName().get_class($e->getTarget());
//                 }
//             );

//             /*
//              * 
//              * */
//             $SEM->attach(
//                 'ZfcUser\Form\Login',
//                 'init',
//                 function (EventInterface $e) {
//                     echo $e->getName().get_class($e->getTarget());
//                 }
//             );

//             /*
//              * Инициализация поиска Фильтра
//              * */
//             $SEM->attach(
//                 'ZfcUser\Mapper\User',
//                 'find',
//                 function (EventInterface $e) {
//                     echo $e->getName().get_class($e->getTarget());
//                 }
//             );

//             /*
//              * Инициализация Фильтра
//              * */
//             $SEM->attach(
//                 'ZfcUser\Form\RegisterFilter',
//                 'init',
//                 function (EventInterface $e) {
//                     echo $e->getName().get_class($e->getTarget());
//                 }
//             );

//             $SEM->attach(
//                 'ZfcUser\Service\User',
//                 'register',
//                 function (EventInterface $e) use ($ServiceLocator){
//                     echo $e->getName().get_class($e->getTarget());
//                 }
//             );
        }
    }
}
